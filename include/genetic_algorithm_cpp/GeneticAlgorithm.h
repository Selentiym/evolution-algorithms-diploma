#include <memory>

//
// Created by nikita on 24.12.18.
//

#ifndef GENETIC_ALGORITHM_CPP_GENETICALGORITHM_H
#define GENETIC_ALGORITHM_CPP_GENETICALGORITHM_H

#include <vector>
#include <iostream>
#include <boost/shared_ptr.hpp>
#include "../../src/list/List.h"
#include "Helpers.h"
#include <boost/functional/hash.hpp>

typedef __SIZE_TYPE__ size_t;
typedef unsigned int discrete_gene;
typedef std::vector<discrete_gene> discrete_chromosome;

typedef double continious_gene;
typedef std::vector<continious_gene> continious_chromosome;


class Entity;
template <typename T>
class TemplatedEntity;

typedef TemplatedEntity<discrete_gene> DiscreteEntity;
typedef TemplatedEntity<continious_gene> ContiniousEntity;

class World;
class Component;
class Population;
class Species;
class EntityFactory;
class SpeciesFactory;
class FitnessComputer;
class Initializer;

//typedef List<Entity*> Species;

typedef std::shared_ptr<Entity> EntityPtr;

typedef std::shared_ptr<Species> SpeciesPtr;

typedef List<SpeciesPtr> SpeciesList;
typedef List<EntityPtr> EntityList;

typedef Node<SpeciesPtr>* SpeciesNode;
typedef Node<EntityPtr>* EntityNode;

typedef int iteration_counter;

template <typename T>
using matrix = std::vector<std::vector<T>>;

/**
 * We need a way to iterate only new entities or only old entities inside
 * the current population, but clustering should be performed together.
 */

typedef std::unique_ptr<Population> PopulationPtr;
//typedef std::shared_ptr<Population> PopulationPtr;
typedef unsigned long entityIndex;

class GAResult {
public:
    double optimum;
    iteration_counter iterations_made;
//    double time_spent_per_iteration;
//    bool error;
};

class StoppingCriteria {
public:
    virtual bool stop(World* world) = 0;
};

class StagnationStoppingCriteria: public StoppingCriteria {
public:
    bool stop(World *world) override;

    explicit StagnationStoppingCriteria(iteration_counter period);

protected:
    iteration_counter period;
    iteration_counter i = -1;
    double fitness;
};

class World {
public:
    PopulationPtr population = nullptr;

    iteration_counter getIteration();

    explicit World(EntityFactory *entityFactory);
    void cleanupIteration();

    std::shared_ptr<Entity> getBestIndividual();

    ~World();

protected:
    iteration_counter iteration = 0;
};

class GeneticAlgorithm {
public:
    GeneticAlgorithm(World *world, Initializer *initializer, std::vector<Component *> *components,
                     StoppingCriteria *stoppingCriteria = nullptr, int numIterations = 3);

    GAResult *RunEvolution();
protected:
    std::vector<Component*>* components;
    World* world;
    iteration_counter numIterations;
    std::unique_ptr<StoppingCriteria> stoppingCriteria;

};

class Population: public SpeciesList {
public:
    Population() = default;

    explicit Population(EntityList *entities);

    Population(Population &previous);

    ~Population() override;
};

typedef std::unique_ptr<EntityList> EntityListPtr;

class Species {
public:
    EntityListPtr common;
    EntityListPtr mutated;
    EntityListPtr crossovered;

    Species();

    Species(Species &source);

    double getFitness();

    void setFitness(double fitness);

    virtual ~Species() = default;

protected:
    bool fitnessSet = false;
    double fitness = 0.0;

    virtual double calculateFitness() = 0;
};

class Initializer {
public:
    explicit Initializer(entityIndex initialSize);

    EntityList *getEntities();
protected:
    virtual void initializeEntity(Entity *entity) = 0;
//    virtual void doInitializeEntity(Entity *entity) = 0;

    entityIndex initialSize;
};

class DiscreteEntityInitializer: public Initializer {
public:
    explicit DiscreteEntityInitializer(entityIndex initialSize);

protected:
    void initializeEntity(Entity *entity) override;

    virtual void doInitializeEntity(DiscreteEntity *entity) = 0;
};

class ContiniousEntityInitializer: public Initializer {
public:
    explicit ContiniousEntityInitializer(entityIndex initialSize);

protected:
    void initializeEntity(Entity *entity) override;

    virtual void doInitializeEntity(ContiniousEntity *entity) = 0;
};


class RandomPermutationInitializer: public DiscreteEntityInitializer {
public:
    explicit RandomPermutationInitializer(entityIndex initialSize);
protected:
    void doInitializeEntity(DiscreteEntity *entity) override;
};

class EntityFactory {
public:
    explicit EntityFactory(FitnessComputer *fitnessComputer);

    void setWorld(World* world);

    virtual Entity *create() = 0;

    virtual Entity *clone(Entity *toClone) = 0;

    static EntityFactory* get();

    static void set(EntityFactory &factory);

    virtual FitnessComputer *getFitnessComputer();

protected:
    World* world;

    static EntityFactory* theFactory;
//    EntityFactory *entityFactory;
//    SpeciesFactory *speciesFactory;
    FitnessComputer *fitnessComputer;
};

class DiscreteEntityFactory: public EntityFactory {
public:
    explicit DiscreteEntityFactory(FitnessComputer *fitnessComputer);

    Entity * create() override;

    Entity * clone(Entity *toClone) override;
};

class SpeciesFactory {
public:
    virtual Species *create() = 0;

    virtual Species *clone(Species *toClone)= 0;

    static SpeciesFactory* get();

    static void set(SpeciesFactory &factory);

protected:
    static SpeciesFactory* theFactory;
};

class AverageFitnessSpeciesFactory: public SpeciesFactory {
public:
private:
    Species *create() override;

    Species *clone(Species *toClone) override;
};

class AverageFitnessSpecies: public Species {
protected:
    double calculateFitness() override;

public:
    AverageFitnessSpecies() = default;

    explicit AverageFitnessSpecies(Species &source);
};

/**********************************************
 *                  Components
 **********************************************/

class Component {
public:
    virtual bool GoForward(World* world) = 0;

    virtual ~Component() = default;
};

class SpeciesWiseComponent: virtual public Component {
public:
    bool GoForward(World* world) override {
        SpeciesNode species;
        bool tmp = true;
        foreach(species, world -> population) {
            tmp &= this->speciesForward(species->val.get(), world);//SPG
        }
        //TODO: add exception may be? Decide Exceptions vs booleans
        return tmp;
    }

    virtual bool speciesForward(Species *species, World *world) = 0;
};

class FitnessTransformer {
public:
    virtual void transform(std::vector<double> &fitnesses) = 0;
};

class RankFitnessTransformer : public FitnessTransformer {
public:
    explicit RankFitnessTransformer(double selection_pressure = -1);
    void transform(std::vector<double> &fitnesses) override;

protected:
    double pressure;
};

class SumToOneFitnessTransformer: public FitnessTransformer {
public:
    void transform(std::vector<double> &fitnesses) override;
};

class MergeMutatedIntoCommon: public SpeciesWiseComponent {
    bool speciesForward(Species *species, World *world) override {
        species -> common -> transferValuesFrom(species -> mutated.get()); // SPG
        return true;
    }
};

class MergeCrossoveredIntoCommon: public SpeciesWiseComponent {
    bool speciesForward(Species *species, World *world) override {
        species -> common -> transferValuesFrom(species -> crossovered.get()); // SPG
        return true;
    }
};

class MergeSpecies: public SpeciesWiseComponent {

    bool GoForward(World* world) {
        SpeciesNode speciesNode;
        auto mergedSpecies = SpeciesFactory::get()->create();
        foreach(speciesNode, world -> population) {
            mergedSpecies -> mutated -> addValuesFrom(speciesNode -> val -> mutated.get()); //SPG
            mergedSpecies -> crossovered -> addValuesFrom(speciesNode -> val -> crossovered.get());
            mergedSpecies -> common -> addValuesFrom(speciesNode -> val -> common.get());
            // delete the Node that stores an empty species
//            delete speciesNode;
        }
        // create a new population and store the merged species in it
        world -> population = std::unique_ptr<Population>(new Population());
        world -> population -> push(SpeciesPtr(mergedSpecies));
//        std::cout << mergedSpecies -> common -> getLength() << std::endl;
        return true;
    }

    bool speciesForward(Species *species, World *world) override {
        species -> common -> transferValuesFrom(species -> crossovered.get()); //SPG
    }
};

class Entity {
public:
    double getRawFitness();

    double getModifiedFitness();

    size_t getHash();

    void setFitness(double fitness);

    iteration_counter birthIteration = 0;

    Entity() = default;

    Entity(EntityFactory *factory, iteration_counter birthIteration);

    Entity(const Entity& source, iteration_counter birthIteration);

    /**
     * @return a pointer to Entity that was mutated to get this Entity
     */
    //TODO: add setParent call into mutation
    Entity* getParent();

    void setParent(Entity * parent);

//    chromosome chr;
protected:
    double rawFitness = 0.0;
    double modifiedFitness = 0.0;
    Entity * parent = nullptr;
    EntityFactory *factory = nullptr;
    bool fitnessComputed = false;
    size_t hash;

    virtual size_t calculateHash() = 0;
    bool hashComputed = false;
};

template <typename T>
class TemplatedEntity: public Entity {
public:
    std::vector<T> chr;

    explicit TemplatedEntity(std::vector<T> path) {
        this -> chr = path;
    }

    TemplatedEntity(EntityFactory *factory, iteration_counter birthIteration) : Entity(factory, birthIteration) {

    }

    TemplatedEntity(const TemplatedEntity<T> &source, iteration_counter birthIteration) : Entity(source, birthIteration) {
        this -> chr = source.chr;
    }

protected:
    size_t calculateHash() override {
        size_t seed = 0;
        boost::hash_combine(seed, boost::hash_value(this -> chr));
        boost::hash_combine(seed, boost::hash_value(this -> getRawFitness()));
        return seed;
//        return ((unsigned long)this -> getRawFitness()) ^ \
//        this -> chr[0]*2654435761*4 \
//        ^ this -> chr[10]*2654435761*2>>15 \
//        ^ this -> chr[20]*2654435761*2>>28 \
//        ^ this -> chr[30]*2654435761*2>>9 \
//        ^ this -> chr[40]*2654435761*2>>22 \
//        ^ this -> chr[50]*2654435761*2>>3 \
//        ;
    }
};

//class DiscreteEntity: public Entity {
//public:
//    discrete_chromosome chr;
//
//
//};
//
//class ContiniousEntity: public Entity {
//public:
//    continious_chromosome chr;
//};

class FitnessComputer {
public:
    virtual double getFitness(Entity* entity) = 0;

    virtual entityIndex getChrSize() = 0;

    virtual double convertToHumanReadableFitness(double fitness) = 0;
};

class TSPFitnessMinusLength: public FitnessComputer {
public:
    explicit TSPFitnessMinusLength(matrix<float> *m);

    double getFitness(Entity* entity) override;

    entityIndex getChrSize() override;

    double convertToHumanReadableFitness(double fitness) override;

protected:
    matrix<float> *matr;
};


class TSPFitnessInversedLength: public TSPFitnessMinusLength {
public:

    double getFitness(Entity* entity) override;

    explicit TSPFitnessInversedLength(matrix<float> *m);

    double convertToHumanReadableFitness(double fitness) override;
};

void normalizeFitnesses(std::vector<double> &fitnesses);

double getAverageFitness(EntityList * entities);

void extractFitnessesFromEntityList(std::vector<double> &fitnesses, EntityList *list, bool raw = false);

void extractFitnessesFromPopulation(std::vector<double> &fitnesses, Population *population);

/**
 * Helper functions that require genetic algorithm types
 * @param individual
 */

void printIndividual(DiscreteEntity *individual);

unsigned long someGenesMissing(DiscreteEntity *individual);

#endif //GENETIC_ALGORITHM_CPP_GENETICALGORITHM_H
