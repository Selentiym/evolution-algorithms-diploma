//
// Created by nikita on 22.11.18.
//

#ifndef CONSOLE_CONFIGURATIONSINGLETON_H
#define CONSOLE_CONFIGURATIONSINGLETON_H

#include "../config4cpp/include/config4cpp/Configuration.h"
#include "../config4cpp/include/config4cpp/ConfigurationException.h"
#include "iostream"
//#include "Helpers/Helpers.h"
#include <vector>
#include "../cnpy/cnpy.h"

template <typename T>
using matrix = std::vector< std::vector<T>>;


template < typename T >
matrix <T> to_2d( const std::vector<T>& flat_vec, std::size_t ncols )
{
    // sanity check
    if( ncols == 0 || flat_vec.size()%ncols != 0 ) throw std::domain_error( "bad #cols" ) ;

    const auto nrows = flat_vec.size() / ncols ;

    matrix <T> mtx ;
    const auto begin = std::begin(flat_vec) ;
    for( std::size_t row = 0 ; row < nrows ; ++row ) mtx.push_back( { begin + row*ncols, begin + (row+1)*ncols } ) ;
    return mtx ;
}


template < typename T >
matrix <T> read_matr(std::string filename)
{
    /**
     * Beware of the type! changing the type populated to the
     * template changes the read completely!
     * numpy.ndarray that has been written to the filename
     * should have the same type
     */
    cnpy::NpyArray arr = cnpy::npy_load(filename);

    std::vector<T> loaded_data = arr.as_vec<T>();

    matrix <T> matr = to_2d(loaded_data, arr.shape[1]);
//    for (const auto& row : matr) {
//        for (T v : row) std::cout << v << ' ';
//        std::cout << '\n';
//    }
    std::cout << "read matrix from " << filename << std::endl;
    return matr ;
}


class ConfigurationSingleton {
public:
    static ConfigurationSingleton &getInstance() {
        static ConfigurationSingleton instance;
        return instance;
    }

    void setProblemName(const std::string &name);

    std::string getInputFilename();


    template < typename T >
    matrix<T> *getMatr();

private:
    CONFIG4CPP_NAMESPACE::Configuration *  mCfg;
    std::string  mInputFileName;

    ConfigurationSingleton() {
        const char *     scope = "";
        std::string     tmp;
        mCfg = CONFIG4CPP_NAMESPACE::Configuration::create();
        try {
            mCfg -> parse("input/evolution.cfg");
            tmp = mCfg -> lookupString(scope, "inputFilename");

            mInputFileName = (std::string("input/") + std::string(tmp));
        } catch(const CONFIG4CPP_NAMESPACE::ConfigurationException & ex) {
            std::cerr << ex.c_str() << std::endl;
            mCfg -> destroy();
        }
        mCfg -> destroy();
    }
};

template <typename T>
matrix<T> *ConfigurationSingleton::getMatr() {
    static matrix <T> matr = read_matr <T>(mInputFileName);
    return &matr;
}

std::string ConfigurationSingleton::getInputFilename() {
    return mInputFileName;
}

void ConfigurationSingleton::setProblemName(const std::string &name) {
    mInputFileName = std::string("input/") + name + std::string(".npy");
}


#endif //CONSOLE_CONFIGURATIONSINGLETON_H
