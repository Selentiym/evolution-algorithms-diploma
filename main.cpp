#include <iostream>
#include <GeneticAlgorithm.h>
#include "src/list/List.h"
#include "boost/shared_ptr.hpp"
#include "../../libs/configuration/ConfigurationSingleton.h"
#include "mutations/mutations.h"
#include "crossovers/crossoversAbstracts.h"
#include "crossovers/crossovers.h"
#include "selections/selections.h"
#include "clustering/clustering.h"
#include "loggers/loggers.h"
#include <map>
#include <chrono>
//#include <Eigen/Dense>

int genitor_alg(long num) {
    std::vector<Component*> components;

    std::unique_ptr<FitnessComputer> fitness (new TSPFitnessInversedLength(ConfigurationSingleton::getInstance().getMatr<float>()));
    components.push_back(new ProbabilisticMutation(new DMMutation(), false, 0.01 / num));
//    components.push_back(new SingleMutation(new DMMutation(), false));
    components.push_back(new MergeMutatedIntoCommon());

    components.push_back(new RollOfFortuneCrossover(new EdgeRecombinationCrossoverCore(), 0, nullptr));
    components.push_back(new MergeCrossoveredIntoCommon());

    components.push_back(new BestReplaceWorstSelection(false, 200));

    std::unique_ptr<EntityFactory> entityFactory (new DiscreteEntityFactory(fitness.get()));
    std::unique_ptr<SpeciesFactory> speciesFactory (new AverageFitnessSpeciesFactory());

    EntityFactory::set(*entityFactory);

    SpeciesFactory::set(*speciesFactory);



    std::unique_ptr<World> world (new World(entityFactory.get()));
    std::unique_ptr<Initializer> initializer(new RandomPermutationInitializer(200));
    std::unique_ptr<GeneticAlgorithm> algorithm(
            new GeneticAlgorithm(world.get(), initializer.get(),
                    &components, nullptr, num));

    auto res = algorithm -> RunEvolution();
    std::cout<<
    EntityFactory::get() -> getFitnessComputer() -> convertToHumanReadableFitness(res -> optimum)
    << std::endl << res -> iterations_made;
    delete res;
    for (auto component: components) {
        delete component;
    }

    return 0;
}


int speciation_alg(DiscreteMutationCore *mut, DiscreteCrossoverCore *cross, DistanceMetric *metric, bool speciation = false,
                   FitnessModifier *modifier = nullptr, size_t populationSize = 200, bool store_individuals = false,
                   bool trackDead = false) {

    size_t num = 3000;

    std::vector<Component*> components;

    std::unique_ptr<FitnessComputer> fitness (new TSPFitnessInversedLength(ConfigurationSingleton::getInstance().getMatr<float>()));


    std::unique_ptr<IndividualsLogger> logger_core(new IndividualsLogger());
    if (store_individuals) {
//        components.push_back(new GenerationalSpeciesWiseLogger(logger_core.get(), {0, 1, 10, 30, 100, 300, 500, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 9998}));
        components.push_back(new EachGenerationSpeciesWiseLogger(logger_core.get()));
    }

    // mutation in each species
    components.push_back(new ProbabilisticMutation(mut, false, 0.2));
    components.push_back(new MergeMutatedIntoCommon());

    // crossover in each species
//    components.push_back(new FixedNumberCrossover(cross, 50, populationSize));
    components.push_back(new ProbabilisticCrossover(cross, 0.4));
    components.push_back(new MergeCrossoveredIntoCommon());

    // merge
    components.push_back(new MergeSpecies());

    // speciation
    if (speciation) {
        components.push_back(new DBScan(metric, 0.15, 4));
        if (modifier != nullptr) {
            components.push_back(modifier);
        }
    }


    components.push_back(new TournamentSelection(false, populationSize, 3, new RankFitnessTransformer(1.5), 1));
    if (trackDead) {
        components.push_back(new SpeciesMatcher(metric, 0.05, 0.9));
    }

    std::unique_ptr<EntityFactory> entityFactory (new DiscreteEntityFactory(fitness.get()));
    std::unique_ptr<SpeciesFactory> speciesFactory (new AverageFitnessSpeciesFactory());

    EntityFactory::set(*entityFactory);

    SpeciesFactory::set(*speciesFactory);


    std::unique_ptr<World> world (new World(entityFactory.get()));
    std::unique_ptr<Initializer> initializer(new RandomPermutationInitializer(populationSize));
    std::unique_ptr<GeneticAlgorithm> algorithm(
            new GeneticAlgorithm(world.get(), initializer.get(),
                                 &components, new StagnationStoppingCriteria(300), num));

    auto res = algorithm -> RunEvolution();
    std::cout<<"fitness, iterations, duration(ms)"<<std::endl;
    std::cout<< EntityFactory::get() -> getFitnessComputer() -> convertToHumanReadableFitness(res -> optimum) << '\n';
    std::cout<< res -> iterations_made<<'\n';
    auto bestIndividual = world -> getBestIndividual();
    printIndividual((DiscreteEntity *) bestIndividual.get());
//    std::cout << "total distance queries " << wrapper -> queries << ", hits: " << wrapper -> hits << std::endl;
    delete res;
    for (auto component: components) {
        delete component;
    }

    return 0;
}


int main(int argc, char* argv[]) {

    std::string problem;

    // deliberately up here, since I have to initialize configuration first
    if (argc > 3) {
        problem = std::string(argv[3]);
    } else {
        problem = std::string("eil51");
    }

    std::cout << "The problem is " << problem <<std::endl;

    ConfigurationSingleton::getInstance().setProblemName(problem);

    DiscreteMutationCore *mut;
    int mutNum = 1;
    if (argc > 1) {
        mutNum = std::stoi(argv[1]);
    }
    switch (mutNum) {
        case 1:
            std::cout << "using SIM"<<std::endl;
            mut = new SIMMutation(); break;
        case 2:
            std::cout << "using DM"<<std::endl;
            mut = new DMMutation(); break;
        default:
            std::cout << "Invalid mutation specified!";
            return 1;
    }

    DiscreteCrossoverCore *cross;
    int crossNum = 1;
    if (argc > 2) {
        crossNum = std::stoi(argv[2]);
    }
    switch (crossNum) {
        case 1:
            std::cout << "using ROX"<<std::endl;
            cross = new RotationalOrderedCrossoverCore(ConfigurationSingleton::getInstance().getMatr<float>()); break;
        case 2:
            std::cout << "using DPX"<<std::endl;
            cross = new DistancePreservingCrossoverCore(ConfigurationSingleton::getInstance().getMatr<float>()); break;
        case 3:
            std::cout << "using ER"<<std::endl;
            cross = new EdgeRecombinationCrossoverCore(); break;
        default:
            std::cout << "Invalid crossover specified!";
            return 1;
    }
    std::unique_ptr<DiscreteCrossoverCore> cross_ptr(cross);

    // with negative fitness
//    std::unordered_map<std::string, double> correction_map({
//        {"eil51", -10.0},
//        {"rat99", -30.0},
//        {"rat195", -55.0},
//        {"kroA100", -500.0}
//    });
    // with inversely proportional fitness
    std::unordered_map<std::string, double> correction_map({
        {"eil51", -5*pow(10.0,-5)},
        {"rat99", -2*pow(10.0,-5)},
        {"rat195", -8*pow(10.0,-6)},
        {"kroA100", -8*pow(10.0,-7)}
    });


    DistanceMetric * metric = new EdgeJaccard(ConfigurationSingleton::getInstance().getMatr<float>()->size());
    auto metric_wrapper = std::unique_ptr<CachingWrapper>(new CachingWrapper(metric));

    FitnessModifier * modifier = nullptr;

    bool speciation;
    int speciation_selector = 1;
    if (argc > 4) {
        speciation_selector = std::stoi(argv[4]);
//        std::cout << argv[4];
    }
    speciation = (bool)speciation_selector;

    std::unique_ptr<PredatorPreyAssigner> assigner;
    double gamma = 0.05;
    if (argc > 6) {
        gamma = std::stod(argv[6]);
    }
    double alphaMultiplier = 0.05;
    if (argc > 7) {
        alphaMultiplier = std::stod(argv[7]);
    }
    switch (speciation_selector) {
        case 0:
            std::cout << "no speciation"<<std::endl; break;
        case 1:
            std::cout << "simple speciation"<<std::endl; break;
        case 2:
            std::cout << "speciation with repelling modifier independently of fitness"<<std::endl;
            modifier = new FitnessIndependentDistanceModifier(metric_wrapper.get(), correction_map[problem], 1, 0.2); break;
        case 3:
            std::cout << "speciation with repelling depending on fitness"<<std::endl;
            modifier = new FitnessDistanceModifier(metric_wrapper.get(), - gamma * alphaMultiplier, 1, 0.2); break;
        case 4:
            std::cout << "speciation with random predator prey"<<std::endl;
            assigner = std::unique_ptr<PredatorPreyAssigner>(new RandomAssigner(0.2));
            modifier = new PredatorPreyDistanceModifier(metric_wrapper.get(), gamma, 1, 0.2, assigner.get(), alphaMultiplier); break;
        case 5:
            std::cout << "speciation with maxEntropy predator prey"<<std::endl;
            assigner = std::unique_ptr<PredatorPreyAssigner>(new MaxEntropyAssigner(gamma));
            modifier = new PredatorPreyDistanceModifier(metric_wrapper.get(), gamma, 1, 0.2, assigner.get(), alphaMultiplier); break;
        case 6:
            std::cout << "speciation with maxEntropy and linear system"<<std::endl;
            assigner = std::unique_ptr<PredatorPreyAssigner>(new MaxEntropyAssigner(gamma));
            modifier = new SLAEModifier(metric_wrapper.get(), gamma, 1, 0.2, assigner.get(), false, alphaMultiplier); break;
        case 7:
            std::cout << "speciation with maxEntropy and linear system with running"<<std::endl;
            assigner = std::unique_ptr<PredatorPreyAssigner>(new MaxEntropyAssigner(gamma));
            modifier = new SLAEModifier(metric_wrapper.get(), gamma, 1, 0.2, assigner.get(), true, alphaMultiplier); break;
        default:
            std::cout << "Invalid speciation specified!";
            return 1;
    }

    size_t population_size = 50;
    if (argc > 5) {
        population_size = std::stoul(argv[5]);
//        std::cout << argv[5];
    }
    std::cout << "the population size is "<< population_size <<std::endl;

    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    int rez;
    rez = speciation_alg(mut, cross, metric_wrapper.get(), speciation, modifier, population_size, true, false);
    std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();

    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>( t2 - t1 ).count();

//    std::cout <<std::endl<<"Duration was: "<< duration << " milliseconds";
    std::cout<< duration << std::endl;

    return rez;
}
