import os
import joblib
from pathtocpp import get_cpp_relative_folder_path

os.chdir(get_cpp_relative_folder_path())

# subprocess.call([])

problems = ["eil51"]

crossovers = {
    "1": "ROX",
    # "2": "DPX",
    # "3": "ER" # tooo loooong
}

mutations = {
    "1": "SIM",
    # "2": "DM"
}

speciation = {
     "0": "no_speciation",
     "1": "with_speciation",
    # "2": "fitness-unaware-repelling_speciation",
    # "3": "fitness-aware-repelling_fitness_speciation",
     "4": "random-predator-prey_speciation",
     "5": "maxentropy-predator-prey_speciation",
     "6": "maxentropy-SLAE-no-running_speciation",
     "7": "maxentropy-SLAE-with-running_speciation"
}

# gammas = [0.01, 0.025, 0.05, 0.075, 0.1, 0.25, 0.5]
# alphas = [0.05, 0.1, 0.25, 2.0]

gammas = [0.15, 0.5, 0.75]
alphas = [0.05]

sizes = {
    # sz: sz for sz in reversed([20, 50, 100, 200, 300, 500])
    sz: sz for sz in [20,50]
}

repeats = [0, 1, 2, 3, 4, 5, 6]
n_jobs = 15


def launch_algorithm(mut, cross, problem, ind=0, spec=0, pop_size=200, gamma=0.1, alphaMod=0.1):
    base_name = "%s_%s_%s_%s_pop_%i_%f_%f" % (problem, mutations[mut], crossovers[cross], speciation[spec], pop_size, gamma, alphaMod)
    foldername = "results/" + base_name
    if not os.path.exists(foldername):
        os.makedirs(foldername)
    flname = "%s_%i.out" % (base_name, ind)
    if os.path.exists("%s/%s" % (foldername, flname)):
        print("The file %s exists, skipping!" % flname)
        return
    print("Launching %s with ind %i" % (base_name, ind))
    # print('./cmake-build-debug/genetic_algorithm_cpp %s %s %s %s > %s/%s' % (mut, cross, problem, spec,foldername, flname))
    os.system('./cmake-build-debug/genetic_algorithm_cpp %s %s %s %s %i %3.1f %3.1f> %s/%s' % (mut, cross, problem, spec, pop_size, gamma, alphaMod, foldername, flname))


jobs = []

for m in mutations:
    for c in crossovers:
        for p in problems:
            for sz in sizes:
                for ind in repeats:
                    for spec in speciation:
                        for gamma in gammas:
                            for alpha in alphas:
                                alphaMod = alpha / gamma
                                jobs.append(joblib.delayed(launch_algorithm)(m, c, p, ind, spec, sz, gamma, alphaMod))

print("Jobs total: %i" % len(jobs))

joblib.Parallel(n_jobs)(jobs)

# os.system('./cmake-build-debug/genetic_algorithm_cpp > results/test.out')
