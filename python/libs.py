import subprocess
import numpy as np
# from scipy.spatial.distance import pdist
import re
import networkx as nx
from pathtocpp import get_cpp_relative_folder_path


def graph_from_tsplib(flname, dtype=int):
    coords = []
    with open(flname, 'r') as fl:
        lines = fl.readlines()
        flag = False
        for ln in lines:
            if ln[:3] == 'EOF':
                break
            if not flag and ln[:4] == 'NODE':
                flag = True
                continue
            if flag:
                tmp = ln.split()
                coords.append((dtype(tmp[1]), dtype(tmp[2])))
    return np.array(coords)


def extract_from_tsplib(flname):
    if flname[-2:] == 'gz':
        print('Extracting %s' % (flname,))
        bashCommand = "gunzip "+flname
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()
        print(output, error)
        return flname[:-3]
    else:
        return flname

# def distance_matrix_by_points_scipy(points):
#     return pdist(points,metric='euclidean')


def matrix_to_graphviz(matr, flname):
    """
    Stores a graph represented by a numpy matrix in a graphviz format
    :param matr:
    :type matr:np.ndarray
    :return:
    """
    pass


def distance_matrix_by_points(points):
    rez = np.zeros((len(points),len(points)))
    for i, pti in enumerate(points):
        for j, ptj in enumerate(points[i:]):
            rez[i,i+j] = np.sqrt(np.sum((pti - ptj) ** 2))
            rez[i+j, i] = rez[i, i+j] # make the matrix symmetrical
    return rez


def weights_matrix_to_shortest_paths(dists):
    G = nx.from_numpy_array(dists)
    ln = len(G.nodes())
    matr = np.zeros((ln, ln))
    for source, dct in nx.shortest_path_length(G, weight='weight'):
        for traget, w in dct.items():
            matr[source, traget] = w
    return matr

# print(distance_matrix_by_points(graph_from_tsplib(extract_from_tsplib('data/att48.tsp'))))


def calculateLength(matr, path):
    prev = path[-1]
    rez = 0
    for cur in path:
        print(matr[prev - 1, cur - 1])
        rez += matr[prev - 1, cur - 1]
        prev = cur
    return rez


def parseOptimalPath(flname):
    rez = []
    with open(flname, 'r') as fl:
        lines = fl.readlines()
        flag = False
        for ln in lines:
            if flag and ln[:2] == '-1':
                break
            if not flag and ln[:4] == 'TOUR':
                flag = True
                continue
            if flag:
                rez.append(int(ln))
        return rez


class KeyHolder:
    def __init__(self, keys, name=''):
        self._keys = keys
        self._reverse = {key: i for i, key in enumerate(keys)}
        self._name = name

    def get_ind(self, key):
        try:
            return self._reverse[key]
        except KeyError as e:
            print("The holder is %s, trying to get the key '%s'" % (self, key))
            raise e

    def __len__(self):
        return len(self._keys)

    def add(self, key):
        if self._reverse.get(key, None) is None:
            self._reverse[key] = len(self._keys)
            self._keys.append(key)

    def get_keys(self):
        return self._keys

    def get_name(self):
        return self._name

    def __str__(self):
        return self._name + ": " + str(self._keys)

    def __getitem__(self, item):
        return self.get_ind(item)

    def __iter__(self):
        return iter(self.get_keys())


class NumpyDictWrapper:
    def __init__(self, holders, max_iter=1000):
        lengths = [len(holder) for holder in holders]
        lengths.append(max_iter)
        self._arr = np.full(lengths, np.nan)
        self._holders = holders
        self._holders_map = {holder.get_name(): holder for holder in holders}

    def add(self, keys, arr):
        if len(arr) != self._arr.shape[-1]:
            print("The array size is incorrect, wanted %i, but had %i" % (self._arr.shape[-1], len(arr)))
        inner_arr = self._get_by_keys(keys)
        inner_arr[:] = arr[:]

    def _get_by_keys(self, keys):
        inner_arr = self._arr
        for i, key in enumerate(keys):
            try:
                inner_arr = inner_arr[self._holders[i].get_ind(key)]
            except IndexError as e:
                print("The holder is %s, the key is '%s', current shape is %s" % (self._holders[i], key, inner_arr.shape))
                raise e
        return inner_arr

    def get_arr(self):
        return self._arr

    def get_mean_over_repeats(self, keys):
        return np.nanmean(self._get_by_keys(keys))

    def holder(self, name) -> KeyHolder:
        return self._holders_map[name]


regex = re.compile("""iteration #\d+ ended, best fitness -([\d\.]+), clusters: (\d+)""")
named_regex = re.compile("""iteration #(?P<iter>\d+) ended, best fitness -?(?P<fit>[\d\.]+), clusters: (?P<clust>\d+)""")


def parse_fitness_line(line):
    match = named_regex.match(line)
    return int(match['iter']), float(match['fit']), int(match['clust'])


basic = re.compile("[^_\.]+")


def parse_string(filename):
    keys = basic.findall(filename)
    problem = keys[0]
    mut = keys[1]
    cross = keys[2]
    spec_key = keys[3]
    size = keys[-3]
    ind = keys[-2]
    return problem, mut, cross, spec_key, size, ind


class Species:
    def __init__(self):
        self._individuals = []
        self._sorted = False
        self._fitnesses = []

    def add(self, path):
        self._individuals.append(path)

    def __len__(self):
        return len(self._individuals)

    def __iter__(self):
        return iter(self._individuals)

    def get_best_individual(self, p: 'Problem'):
        if not self._sorted:
            fitnesses = [p.calculate_length(ind) for ind in self._individuals]
            idxs = np.argsort(fitnesses)
            self._individuals = [self._individuals[ind] for ind in idxs]
            self._fitnesses = [fitnesses[ind] for ind in idxs]
            self._sorted = True
        return self._individuals[0]


class Population:
    def __init__(self, iter=-1):
        self._current_species = None
        self._current_species = None
        self._species = []
        self.iter = iter

    def next_species(self):
        if self._current_species is not None and len(self._current_species) > 0:
            self._species.append(self._current_species)
        self._current_species = Species()

    def add_individual(self, path):
        self._current_species.add(path)

    def get_species(self):
        self.next_species()  # close the current species if some is opened
        return self._species


class PopulationHolder:
    def __init__(self):
        self._cur_pop = None
        self.populations = []

    def next_population(self, iter=-1):
        if self._cur_pop is not None:
            self.populations.append(self._cur_pop)
        self._cur_pop = Population(iter)

    def get_cur_pop(self) -> Population:
        return self._cur_pop


number_regexp = re.compile("\d+")


def parse_individual_line(line):
    return [int(x) for x in number_regexp.findall(line)]


def parse_file(full_path, max_iterations, skiprows=6):
    rez = np.full(max_iterations, np.nan, dtype='float32')
    ph = PopulationHolder()
    FITNESS_MODE = 1
    INDIV_MODE = 2
    with open(full_path, 'r') as fl:
        lines = fl.readlines()
    last_mode = FITNESS_MODE
    iter = 0
    for line in lines[skiprows:]:
        if line[:20] == 'fitness, iterations,':
            break
        if line.find('iteration') == 0:
            iter, length, clusters = parse_fitness_line(line)
            rez[iter - 1] = length
            last_mode = FITNESS_MODE
        else:
            if last_mode == FITNESS_MODE:
                ph.next_population(iter)
            # if the line start with 'species' keyword, then the next species should be started
            if line.find('species') == 0:
                # new_species = number_regexp.findall(line)[0]  # attempt to parse number
                ph.get_cur_pop().next_species()
            # if the line contains a list, just parse it
            if line.find('[') == 0:
                try:
                    path = parse_individual_line(line)
                    ph.get_cur_pop().add_individual(path)
                except:
                    print('Parse error for %s' % line)
            last_mode = INDIV_MODE
    rez[np.isnan(rez)] = np.nanmin(rez)
    return rez, ph.populations


def parse_file_deaths(full_path, max_iterations):
    rez = np.full((max_iterations, 4), np.nan, dtype='uint8')
    with open(full_path, 'r') as fl:
        text = fl.read()
    regex = re.compile('Was \d+ species, died: (?P<died>\d+), split: (?P<split>\d+), born (?P<born>\d+), merged (?P<merged>\d+)')
    i = 0
    for tup in re.findall(regex, text):
        rez[i] = np.array(tup)
        i += 1
    return rez


def parse_array(full_path, read_lines, skiprows=6):
    rez = np.full(read_lines, np.nan, dtype='float32')
    with open(full_path, 'r') as fl:
        for i, line in enumerate(fl):
            if i < skiprows:
                continue
            if i >= read_lines + skiprows:
                break
            try:
                iter, length, clusters = parse_fitness_line(line)
                rez[iter - 1] = length
            except:
                break
    # set the fitness of the non-filled itrations to be equal to the best fitness
    rez[np.isnan(rez)] = np.nanmin(rez)
    return rez


def get_line(fname, row):
    with open(fname, 'rb') as fh:
        fh.seek(-8192, 2)  # puth the pointer to 8192 bytes before the end of file so that
        # we did not need to read the whole file and used only the last few lines.
        # note that the bigger the row is, the more probability that we may put the pointer
        # _before_ the target row and get an index error
        return fh.readlines()[-row].decode()


def get_final_fitness(fname):
    try:
        from_file = get_line(fname, 4)
        return float(from_file)
    except:
        return None


# https://stackoverflow.com/questions/25482876/how-to-add-legend-to-imshow-in-matplotlib
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
def plot_best_method_colorbar(wrapper, keys):
    """
    :arr: array of shape (2, <number of samples for size>, <number of random seeds>, <max number of generations>)
    """
    arr = wrapper._get_by_keys(keys)
    averaged = np.nanmean(arr, axis=-2)  # average over the trials
    data = np.argmin(averaged, axis=0)  # get the shortest found path

    values = np.unique(data.ravel())

    fig, ax = plt.subplots(figsize=(8, 4))
    im = plt.imshow(data, interpolation='none', aspect='auto')

    # get the colors of the values, according to the
    # colormap used by imshow
    colors = [im.cmap(im.norm(value)) for value in values]
    # create a patch (proxy artist) for every color
    patches = [
        mpatches.Patch(color=colors[i], label="{method}".format(method=wrapper.holder('speciation').get_keys()[i])) for
        i in range(len(values))]

    ax.invert_yaxis()
    row_labels = wrapper.holder('size').get_keys()
    ax.set_yticklabels(row_labels, minor=False)

    ax.set_yticks(np.arange(data.shape[0]), minor=False)

    plt.title(str(keys))

    # put those patched as legend-handles into the legend
    plt.legend(handles=patches, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

    plt.grid(True)
    plt.show()


def plot_fitness_history(arr, holder, colors=None, ax=None):
    if ax is None:
        obj = plt
        plt.xscale('log')
    else:
        obj = ax
        ax.set_xscale('log')
    for k in holder:
        if colors is None:
            obj.plot(arr[holder[k]], label=k, linewidth=3)
        else:
            obj.plot(arr[holder[k]], label=k, linewidth=3, c=colors[k])
    # plt.plot(arr[0], label='no')
    # plt.plot(arr[1], label='with')
    # plt.plot(arr[2], label='repelling')
    obj.legend()
    if ax is None:
        plt.show()


def draw_by_population_sizes(dict_wrapper: NumpyDictWrapper, keys, colors=None):
    tmp = dict_wrapper._get_by_keys(keys)  # load the array of trials for current
    averaged = np.nanmean(tmp, axis=-2)
    argmin = np.argmin(averaged, axis=0)
    np.count_nonzero(np.isnan(argmin))
    holder = dict_wrapper.holder('size')
    for key in holder.get_keys():
        Problem.get(keys[0]).prepare_plot_for_fitness()
        plt.title(key)
        plot_fitness_history(averaged[:, holder.get_ind(key), :], holder=dict_wrapper.holder('speciation'), colors=colors)
    plt.show()


def add_jitter(x, mult=0.1):
    x += np.random.uniform(-0.5, 0.5, len(x)) * np.std(x) * mult


class Problem:
    _problems = {}

    def __init__(self, name: str, dtype=int, round_dist=True):
        self._optimal_length = None
        self._name = name
        self._round = round if round_dist else lambda x: x
        self._coords = []
        self._matr = None
        self._dtype = dtype
        self._parse_file()
        self._path = None
        self._parse_optimal_path()

    def _construct_name(self):
        return 'data/%s' % self._name

    def _parse_file(self):
        self._coords = []
        with open(self._construct_name() + '.tsp', 'r') as fl:
            lines = fl.readlines()
            flag = False
            for ln in lines:
                if ln[:3] == 'EOF':
                    break
                if not flag and ln[:4] == 'NODE':
                    flag = True
                    continue
                if flag:
                    tmp = ln.split()
                    self._coords.append((self._dtype(tmp[1]), self._dtype(tmp[2])))
        self._coords = np.array(self._coords)
        n = len(self._coords)
        self._matr = np.zeros((n, n))
        for i in range(n):
            for j in range(i + 1, n):
                self._matr[i, j] = self._round(np.sum((self._coords[i] - self._coords[j]) ** 2) ** 0.5)
                self._matr[j, i] = self._matr[i, j]

    def get_coords_tuples(self, path):
        add = -np.min(path)
        return np.array([self._coords[i + add][0] for i in path] + [self._coords[path[0]+add][0]], dtype='float32'), np.array(
            [self._coords[i + add][1] for i in path] + [self._coords[path[0] + add][1]], dtype='float32')

    def _parse_optimal_path(self):
        self._path = np.array(parseOptimalPath(self._construct_name() + '.opt.tour'))
        self._path -= np.min(self._path)

    def get_optimal_path(self):
        return self._path

    def calculate_length(self, path):
        prev = path[-1]
        rez = 0
        #         rez = -self._matr[prev, path[0]]  # to get 417.98 for eil on that strange guy's solution
        for cur in path:
            rez += self._matr[prev, cur]
            prev = cur
        return rez

    def get_optimal_length(self):
        if self._optimal_length is None:
            p = self.get_optimal_path()
            self._optimal_length = self.calculate_length(p)
        return self._optimal_length

    def plot(self, path, jitter=True):
        x, y = self.get_coords_tuples(path)
        if jitter:
            add_jitter(x, mult=0.05)
            add_jitter(y, mult=0.05)
        l1, = plt.plot(x, y)
        l2 = plt.scatter(x, y)
        return l1, l2

    def plot_with_optimal(self, path):
        self.plot(self.get_optimal_path())
        self.plot(path)

    def store_cpp_matrix(self):
        save_filename = get_cpp_relative_folder_path() + "input/%s.npy" % self._name
        np.save(save_filename, self._matr.astype('float32'))

    def prepare_plot_for_fitness(self, ax=None):
        m = self.get_optimal_length()
        limits = [m * 0.995, 1.2 * m]
        if ax is None:
            obj = plt
            plt.ylim(limits)
        else:
            obj = ax
            ax.set_ylim(limits)
        obj.axhline(m, xmin=0, xmax=1, c='red', linestyle='--', linewidth=1)

    @classmethod
    def get(cls, key):
        try:
            p = cls._problems[key]
        except KeyError:
            p = cls(key)
            cls._problems[key] = p
        return p


def inversion_count_similarity(data1, data2):
    N = len(data1)
    o1 = np.argsort(data1, kind='mergesort')
    o2 = np.argsort(data2, kind='mergesort')
    o1inv = np.empty_like(o1)
    o1inv[o1] = np.arange(N)
    # pad to power of two
    order = np.arange(1<<N.bit_length())
    order[:N] = o2[o1inv]

    sum_ = 0
    for i in range(1, N.bit_length()+1):
        order = np.reshape(order, (-1, 1<<i))
        oo = np.argsort(order, axis = -1, kind='mergesort')
        ioo = np.empty_like(oo)
        ioo[np.arange(order.shape[0])[:, None], oo] = np.arange(1<<i)
        order[...] = order[np.arange(order.shape[0])[:, None], oo]
        hw = 1<<(i-1)
        sum_ += ioo[:, :hw].sum() - order.shape[0] * (hw-1)*hw // 2
    return 1 - (2*sum_)/((N-1)*N)


def edge_jaccard_similarity(x, y):
    n = len(x)
    # will contain numbers of cities that are next to the index city in both x's and y's path
    neighbours = np.zeros((n, 2), dtype='int32')
    prev = n - 1
    for i in range(n):
        neighbours[x[i]-1, 0] = x[prev]
        nxt = (i + 1) % n
        neighbours[x[i]-1, 1] = x[nxt]
        prev = i
    # remove the x's neighbours that are not present in y
    prev = n - 1
    counter = 0
    for i in range(n):
        nxt = (i + 1) % n
        # remove the first neighbour of the current city if it is not the neighbour from y's perspective
        if (neighbours[y[i]-1, 0] != y[prev]) and (neighbours[y[i]-1, 0] != y[nxt]):
            neighbours[y[i]-1, 0] = -1
            counter += 1
        # same for the second neighbour
        if (neighbours[y[i]-1, 1] != y[prev]) and (neighbours[y[i]-1, 1] != y[nxt]):
            neighbours[y[i]-1, 1] = -1
            counter += 1
        prev = i
    # counter contains the number of -1's. The total number of options is 2*n
    # the number of common edges is (2*n - counter)/2, since each edge is counted twice
    commonEdges = n - counter // 2
    return commonEdges / float(2*n - commonEdges)


basic = re.compile("[^_]+")
def parse_string_alpha(filename, ratio=False):
    keys = basic.findall(filename)
    problem = keys[0]
    mut = keys[1]
    cross = keys[2]
    spec_key = keys[3]
    size = keys[-4]
    ind = keys[-1].split('.')[0]
    gamma = float(keys[-3])
    if ratio:
        alpha = round(float(keys[-2]),5)
    else:
        alpha = round(float(keys[-2]) * gamma,5)
    return problem, mut, cross, spec_key, size, ind, gamma, alpha
