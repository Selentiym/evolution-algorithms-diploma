import os
from pathtocpp import get_cpp_relative_folder_path
from libs import parse_string
def get_line(fname, row):
    with open(fname, 'rb') as fh:
        fh.seek(-8192, 2)  # puth the pointer to 8192 bytes before the end of file so that
        # we did not need to read the whole file and used only the last few lines.
        # note that the bigger the row is, the more probability that we may put the pointer
        # _before_ the target row and get an index error
        return fh.readlines()[-row].decode()

def get_final_fitness(fname):
    try:
        from_file = get_line(fname, 4)
        return float(from_file)
    except:
        return None

i = 0
for direct in os.scandir(get_cpp_relative_folder_path()+"results"):
    for fl in os.scandir(direct):
        keys = parse_string(fl.name)
        full_path = get_cpp_relative_folder_path()+"results/" + direct.name + "/" + fl.name
        if get_final_fitness(full_path) is None:
            i += 1
            print(keys)
            print(full_path)
            new_name = full_path.replace('.out', '_incomplete.out')
            os.remove(full_path)
            print(new_name)
print(i)
