//
// Created by nikita on 20.01.19.
//

#include <algorithm>
#include <tuple>
#include "Helpers.h"

bool probaTrial(double proba) {
    if ((proba > 1.0) || (proba < 0.0)) {
        throw "Proba should be no bigger than 1 and no lower than 0.";
    }
    return uniform() < proba;
}

double uniform() {
    static std::random_device rd;  //Will be used to obtain a seed for the random number engine
    static std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
//    static std::mt19937 gen(42); //Standard mersenne_twister_engine seeded a fix number
    static std::uniform_real_distribution<> dis(0.0, 1.0);
    return dis(gen);
}


/**
 * Stores a vector of indexes that make A sorted into map
 * i.e. A[map[0]] < A[map[1]] < A[map[2]] < ...
 * @param A
 * @param map the result will be store here
 */
void rankToIndex(std::vector<double>& A, std::vector<size_t> &map) {
    size_t n = A.size();
    typedef std::tuple<double, size_t> T;
    std::vector<T> tuples;

    // Create array of tuples storing value and index
    for(int j = 0; j < n; ++j) {
        tuples.emplace_back(A[j], j);
    }

    // Sort tuples by data value
    std::sort(begin(tuples), end(tuples), [](T const &t1, T const &t2) {
        return std::get<0>(t1) < std::get<0>(t2); // or use a custom compare function
    });

    size_t r = 0;
    for (auto tup: tuples) {
//        std::cout <<"rank: "<< r << ", value: " << std::get<0>(tup) << ", initial index: " << std::get<1>(tup) << std::endl;
        map[r] = std::get<1>(tup);
        ++r;
    }
}

double max(double a, double b) {
    if (a > b) {
        return a;
    }
    return b;
}
