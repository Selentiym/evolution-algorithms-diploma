//
// Created by nikita on 12.01.19.
//

#ifndef GENETIC_ALGORITHM_CPP_HELPERS_H
#define GENETIC_ALGORITHM_CPP_HELPERS_H

#include <random>
#include <numeric>      // std::iota
#include <algorithm>    // std::sort


double uniform();

bool probaTrial(double proba);

typedef unsigned long ind_helpers;

double kthLargest(std::vector<double> &v, ind_helpers k);
double kthLargest_sort(std::vector<double> &v, ind_helpers k);

double kthSmallest(std::vector<double> &v, ind_helpers k);
double kthSmallest_sort(std::vector<double> &v, ind_helpers k);

template <typename T>
T getRandInt(T max, T min = 0) {
    return (T) floor(uniform() * (max - min) + min);

//    static std::random_device rd;  //Will be used to obtain a seed for the random number engine
//    static std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
//    static std::mt19937 gen(42); //Standard mersenne_twister_engine seeded a fix number
//    static std::uniform_int_distribution<> dis(min, max-1);
//    return dis(gen);
}

void rankToIndex(std::vector<double>& A, std::vector<size_t> &map);

double max(double a, double b);

template <typename T>
std::vector<size_t> sort_indexes_descending(const std::vector<T> &v) {

    // initialize original index locations
    std::vector<size_t> idx(v.size());
    std::iota(idx.begin(), idx.end(), 0);

    // sort indexes based on comparing values in v
    std::sort(idx.begin(), idx.end(),
              [&v](size_t i1, size_t i2) {return v[i1] > v[i2];});

    return idx;
}

#endif //GENETIC_ALGORITHM_CPP_HELPERS_H
