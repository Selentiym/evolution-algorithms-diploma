#include <memory>

//
// Created by nikita on 24.12.18.
//

//#include "GeneticAlgorithm.h"
#include <GeneticAlgorithm.h>
#include <cfloat>

#include <set>
#include <map>
#include "Helpers.h"

#include "../../include/genetic_algorithm_cpp/GeneticAlgorithm.h"
#include "../mutations/mutationsAbstracts.h"
#include "../crossovers/crossovers.h"
#include "../selections/selections.h"


/**
 * Modifies A to contain ranks of elements instead of the elements themselves.
 * Ranks are growing from 1 to A.size().
 * If two or more elements in the initial array are the same, the rank is defined
 * as the mean of ranks that would be given to those elements if they were different.
 * @param A
 */
void rankify(std::vector<double>& A) {
    size_t n = A.size();
    std::vector<double> R(n,0);
    typedef std::vector<double> T;
    std::vector<T> tuples;
    int r = 1;

    // Create array of tuples storing value and index
    for(int j = 0; j < n; j++) {
//        tuples.push_back(A[j], j);
        T temp;
        temp.push_back(A[j]);
        temp.push_back((double)j);
        tuples.push_back(temp);
    }

    // Sort tuples by data value
    std::sort(begin(tuples), end(tuples), [](T const &t1, T const &t2) {
//        return std::get<0>(t1) < std::get<0>(t2); // or use a custom compare function
        return t1[0] < t2[0]; // or use a custom compare function
    });

    size_t i = 0;
    size_t index, j;
    while(i < n) {
        j = i;

        // Get elements of same rank
        while(j < n - 1 && tuples[j][0] == tuples[j+1][0]) {
            j++;
        }

        size_t m = j - i + 1;

        for(j = 0; j < m; j++) {
            // For each equal element use .5
            index = (size_t)tuples[i+j][1];
            R[index] = r + (m-1)*0.5;
        }

        // Increment rank and index
        r+=m;
        i+=m;
    }

    A.swap(R);
}

/**********************************************
 *                  GeneticAlgorithm
 **********************************************/

GAResult *GeneticAlgorithm::RunEvolution() {
    iteration_counter i;
    for (i = 0; i < this -> numIterations; ++i) {
        for (auto component: *this -> components) {
            component -> GoForward(world);
        }
        if (this -> stoppingCriteria) {
            if (this -> stoppingCriteria -> stop(world)) {
                break;
            }
        }
        world -> cleanupIteration();
    }
    auto res = new GAResult();
    res -> iterations_made = world -> getIteration();
    res -> optimum = world->getBestIndividual()->getRawFitness();
    return res;
}

GeneticAlgorithm::GeneticAlgorithm(World *world, Initializer *initializer, std::vector<Component *> *components,
                                   StoppingCriteria *stoppingCriteria, int numIterations) {
    this -> world = world;
    this -> components = components;
    this -> numIterations = numIterations;
    this -> stoppingCriteria = std::unique_ptr<StoppingCriteria>(stoppingCriteria);
    world -> population = std::unique_ptr<Population>(new Population(initializer -> getEntities()));
}


/**********************************************
 *                  World
 **********************************************/


void World::cleanupIteration() {
    ++iteration;
//    if (iteration % 10 == 0) {
//        std::cout << "iteration #"<<iteration<<" ended"<<std::endl;
//    }
    std::cout << "iteration #"<<iteration<<" ended, best fitness "<<
    EntityFactory::get() -> getFitnessComputer() -> convertToHumanReadableFitness(this->getBestIndividual()->getRawFitness())
    << ", clusters: " << this -> population -> getLength() << std::endl;
}

/**********************************************
 *                  Clustering
 **********************************************/

/**********************************************
 *                  Population
 **********************************************/

Population::Population(Population &previous) {
    SpeciesNode species;
    Population *previousPointer = &previous;
    foreach(species, previousPointer) {
        this -> push(
                SpeciesPtr(SpeciesFactory::get() -> clone(species->val.get()))
                );
    }
}

Population::Population(EntityList *entities) {
    Species *species = SpeciesFactory::get() -> create();
    species -> common = std::unique_ptr<EntityList>(entities);
    this -> push(SpeciesPtr(species));
}

Population::~Population() {
//    std::cout << "destroy Population";
}

/**********************************************
 *                  Entity
 **********************************************/


double Entity::getRawFitness() {
    if (not this -> fitnessComputed) {
        this -> fitnessComputed = true;
        this -> rawFitness = this -> factory -> getFitnessComputer() -> getFitness(this);
        this -> modifiedFitness = rawFitness;
    }
    return rawFitness;
}

void Entity::setFitness(double fitness) {
    this -> modifiedFitness = fitness;
}

Entity *Entity::getParent() {
    return parent;
}

void Entity::setParent(Entity *parent) {
    this -> parent = parent;
}

//Entity::Entity(const Entity &source, iteration_counter birthIteration) {
//    throw "Copy constructor in Entity not implemented";
////    this -> birthIteration = birthIteration;
////    this -> chr = source.chr;
////    this -> factory = source.factory;
//}

Entity::Entity(EntityFactory *factory, iteration_counter birthIteration) {
    this -> birthIteration = birthIteration;
    this -> factory = factory;
}

Entity::Entity(const Entity &source, iteration_counter birthIteration) {
    this -> birthIteration = birthIteration;
    this -> factory = source.factory;
}

double Entity::getModifiedFitness() {
    return modifiedFitness;
}

size_t Entity::getHash() {
    if (not this -> hashComputed) {
        this -> hash = this -> calculateHash();
        this -> hashComputed = true;
    }
    return this -> hash;
}

//Entity::Entity(std::vector<discrete_gene> vector) {
//    this -> chr = vector;
//}

/**********************************************
 *                  Population
 **********************************************/

//Population::Population(Population *previous) {
//    //TODO: implement
//}

/**********************************************
 *                  Species
 **********************************************/

//Species::Species(Species *source) {
//    //TODO: implement
//}

/**********************************************
 *                  EntityFactory
 **********************************************/

Entity * DiscreteEntityFactory::create() {
//    EntityPtr tmp(new Entity(this -> world -> getIteration())); //SPG
    auto tmp = new TemplatedEntity<discrete_gene>(this, this->world->getIteration());
    tmp -> chr.resize(this -> fitnessComputer -> getChrSize());
    return tmp;
}

Entity * DiscreteEntityFactory::clone(Entity *toClone) {
    return new DiscreteEntity(* (DiscreteEntity *)toClone, this -> world -> getIteration());
}

DiscreteEntityFactory::DiscreteEntityFactory(FitnessComputer *fitnessComputer) : EntityFactory(fitnessComputer) {

}

void EntityFactory::setWorld(World *world) {
    this -> world = world;
}

EntityFactory *EntityFactory::get() {
    return EntityFactory::theFactory;
}

void EntityFactory::set(EntityFactory &factory) {
    EntityFactory::theFactory = &factory;
}

EntityFactory::EntityFactory(FitnessComputer *fitnessComputer) {
    this -> fitnessComputer = fitnessComputer;
}

iteration_counter World::getIteration() {
    return iteration;
}

World::World(EntityFactory *entityFactory) {
    entityFactory -> setWorld(this);
}

std::shared_ptr<Entity> World::getBestIndividual() {
    double best = -DBL_MAX, fitness;
    std::shared_ptr<Entity> bestIndividual = nullptr;
    SpeciesNode speciesNode;
    EntityNode entityNode;
    // counter is commented out
//    unsigned long cnt = 0;
    foreach(speciesNode, this -> population) {
        foreach(entityNode, speciesNode -> val -> common) {
            fitness = entityNode->val->getRawFitness();
//            ++cnt;
            if (fitness > best) {
                best = fitness;
                bestIndividual = entityNode -> val;
            }
        }
    }
//    std::cout << "The population size is: " << cnt << std::endl;
    return bestIndividual;
}

World::~World() {
//    delete population;
//    delete previousPopulation;
}

/**********************************************
 *                  FitnessComputer
 **********************************************/

TSPFitnessMinusLength::TSPFitnessMinusLength(matrix<float> *m) {
    matr = m;
}


double TSPFitnessMinusLength::getFitness(Entity *entity_outer) {
    float res = 0;
    auto entity = (DiscreteEntity *) entity_outer;
    typedef int indextype; //for this function only
    size_t args_size = entity -> chr.size();

    for (uint i = 1; i < args_size; ++i) {
        res += (*matr)[(indextype)entity -> chr[i-1]][(indextype)entity -> chr[i]];
    }
    res += (*matr)[(indextype) entity -> chr[args_size - 1]][(indextype)entity -> chr[0]];
    return (double)-res;
}

entityIndex TSPFitnessMinusLength::getChrSize() {
    return this -> matr -> size();
}

double TSPFitnessMinusLength::convertToHumanReadableFitness(double fitness) {
    return - fitness;
}

Species::Species(Species &source) {
    this -> mutated = std::unique_ptr<EntityList> (new EntityList(*source.mutated.get()));
    this -> common = std::unique_ptr<EntityList> (new EntityList(*source.common.get()));
    this -> crossovered = std::unique_ptr<EntityList> (new EntityList(*source.crossovered.get()));
}

double Species::getFitness() {
    if (not this -> fitnessSet) {
        this -> setFitness(this -> calculateFitness());
    }
    return this -> fitness;
}

Species::Species() {
    common = std::unique_ptr<EntityList>(new EntityList());
    crossovered = std::unique_ptr<EntityList>(new EntityList());
    mutated = std::unique_ptr<EntityList>(new EntityList());
}

void Species::setFitness(double fitness) {
    this -> fitness = fitness;
    this -> fitnessSet = false;
}


Species *AverageFitnessSpeciesFactory::create() {
    return new AverageFitnessSpecies();
}

Species *AverageFitnessSpeciesFactory::clone(Species *toClone) {
    return new AverageFitnessSpecies(*toClone);
}

double AverageFitnessSpecies::calculateFitness() {
    return getAverageFitness(this -> common.get());
}

AverageFitnessSpecies::AverageFitnessSpecies(Species &source) : Species(source) {

}

SpeciesFactory *SpeciesFactory::get() {
//    if (SpeciesFactory::theFactory == nullptr) {
//        throw "You forgot to specify species factory!";
//    }
    return SpeciesFactory::theFactory;
}

void SpeciesFactory::set(SpeciesFactory &factory) {
    SpeciesFactory::theFactory = &factory;
}

SpeciesFactory* SpeciesFactory::theFactory = nullptr;
EntityFactory* EntityFactory::theFactory = nullptr;

Initializer::Initializer(entityIndex initialSize) {
    this -> initialSize = initialSize;
}

EntityList *Initializer::getEntities() {
    auto rez = new EntityList();
    for (entityIndex i = 0; i < this -> initialSize; ++i) {
        rez -> push(EntityPtr(EntityFactory::get() -> create()));
//        rez -> push(std::shared_ptr<Entity>(entity));
        this -> initializeEntity(rez -> tail -> val.get()); //SPG
    }
    return rez;
}

void RandomPermutationInitializer::doInitializeEntity(DiscreteEntity *entity) {
    // initialize an ordered transposition
    for (int j = 0; j < entity -> chr.size(); ++j) {
        entity -> chr[j] = j;
    }
    int swapInd;
    // perform random mutations
    for (int j = 0; j < entity -> chr.size(); ++j) {
        swapInd = (int) getRandInt(entity -> chr.size());
        std::swap(entity -> chr[j], entity -> chr[swapInd]);
    }
}

RandomPermutationInitializer::RandomPermutationInitializer(entityIndex initialSize) : DiscreteEntityInitializer(
        initialSize) {

}

FitnessComputer *EntityFactory::getFitnessComputer() {
    return fitnessComputer;
}

void RankFitnessTransformer::transform(std::vector<double> &fitnesses) {
    rankify(fitnesses);
    if ((pressure > 1)&&(fitnesses.size() > 1)) {
        auto n = (double)fitnesses.size();
        double bias = (n - pressure * (0.5 + n/2)) / (pressure - 1);
        for (double &fit: fitnesses) {
            fit = (fit + bias) / (n * bias + n * (0.5 + n/2));
        }
    }
}

RankFitnessTransformer::RankFitnessTransformer(double selection_pressure) {
    pressure = selection_pressure;
}

bool StagnationStoppingCriteria::stop(World *world) {
    auto ft = world->getBestIndividual()->getRawFitness();
    if ((i < 0) or (ft > fitness)) {
        fitness = ft;
        i = 0;
    }
    if (period > i) {
        ++i;
        return false;
    }
    return true;
}

StagnationStoppingCriteria::StagnationStoppingCriteria(iteration_counter period) {
    this -> period = period;
}


void normalizeFitnesses(std::vector<double> &fitnesses) {
    double sum = 0;
    for (auto val: fitnesses) {
        sum += val;
    }
    for (double &fitness: fitnesses) {
        fitness /= sum;
    }
}

void extractFitnessesFromEntityList(std::vector<double> &fitnesses, EntityList *list, bool raw) {
    //TODO: move extracting fitnesses into EntityList
    fitnesses.resize(list -> getLength());
    int ind = 0;
    EntityNode entity;
    if (raw) {
        foreach(entity, list) {
            fitnesses[ind] = entity->val->getRawFitness();
            ++ind;
        }
    } else {
        foreach(entity, list) {
            fitnesses[ind] = entity->val->getModifiedFitness();
            ++ind;
        }
    }
}

void extractFitnessesFromPopulation(std::vector<double> &fitnesses, Population *population) {
    auto n = population -> getLength();
    SpeciesNode speciesNode;
    fitnesses.reserve(n);
    // collect fitnesses from species
    foreach (speciesNode, population) {
        fitnesses.push_back(speciesNode -> val -> getFitness());
    }
}


void printIndividual(DiscreteEntity *individual) {
    std::cout << "[";
    for (auto gene: individual -> chr) {
        std::cout << gene << ", ";
    }
    std::cout<<"]" << std::endl;
}

unsigned long someGenesMissing(DiscreteEntity *individual) {
    std::vector<bool> present(individual -> chr.size(), false);
    unsigned long cnt = 0;
    for (auto gene: individual -> chr) {
        if (not present[gene]) {
            present[gene] = true;
        } else {
            ++ cnt;
        }//

    }

//    for (unsigned long j = 0; j < present.size(); ++j) {
//        if (not present[j]) {
//            ++ cnt;
//        }
//    }
    return cnt;
}

double getAverageFitness(EntityList *entities) {
    EntityNode entity;
    double sum = 0;
    foreach(entity, entities) {
        sum += entity->val->getRawFitness();
    }
    return sum / entities -> getLength();
}

DiscreteEntityInitializer::DiscreteEntityInitializer(entityIndex initialSize) : Initializer(initialSize) {

}

void DiscreteEntityInitializer::initializeEntity(Entity *entity) {
    this -> doInitializeEntity((DiscreteEntity *) entity);
}

ContiniousEntityInitializer::ContiniousEntityInitializer(entityIndex initialSize) : Initializer(initialSize) {

}

void ContiniousEntityInitializer::initializeEntity(Entity *entity) {
    this -> doInitializeEntity((ContiniousEntity *) entity);
}

void SumToOneFitnessTransformer::transform(std::vector<double> &fitnesses) {
    double sum = 0.0;
    for (auto fitness: fitnesses) {
        sum += fitness;
    }
    for (auto fitness: fitnesses) {
        fitness /= sum;
    }
}

double TSPFitnessInversedLength::getFitness(Entity *entity) {
    return - 1.0 / TSPFitnessMinusLength::getFitness(entity);
}

TSPFitnessInversedLength::TSPFitnessInversedLength(matrix<float> *m) : TSPFitnessMinusLength(m) {

}

double TSPFitnessInversedLength::convertToHumanReadableFitness(double fitness) {
    return 1 / fitness;
}
