//
// Created by nikita on 15.02.19.
//

#include <GeneticAlgorithm.h>
#include "clustering.h"
#include <unordered_set>

#define CLUSTERED 0
#define UNCLASSIFIED -1
#define NOISE -2

Population * DBScan::doCluster(List<EntityPtr> *entities) {
//    std::cout << entities -> getLength() << '\n';

    // resulting population
    auto list = new Population();
    // the species that is being gathered at the xent
    Species *cluster;
    // array of labels
    std::vector<long> labels(entities -> getLength(), UNCLASSIFIED);

    // a fast way to get entity pointer by its index
    std::vector<std::shared_ptr<Entity>> entitiesArray(entities -> getLength());

    // gather an index of pointers
    Node<EntityPtr> *node;
    unsigned long index = 0;
    foreach(node, entities) {
        entitiesArray[index] = node -> val;
        ++ index;
    }

    // create an array and expand it so it could fit arbitrarily large neighbour set
    // and no memory allocation was needed any more
    std::vector<unsigned long> seed;
    seed.reserve(entities -> getLength());
    // a temporary array for inner usage
    std::vector<unsigned long> tmpNeighbours;
    seed.reserve(entities -> getLength());

    long currentIndex = -1;

    // iterate over non-assigned points
    foreach(node, entities) {
        ++ currentIndex;
        if (labels[currentIndex] != UNCLASSIFIED) {
            continue;
        }
        // find the candidates
        this->findNeighbours(node->val.get(), entities, &seed);
        // if the neighbour set is not large enough, mark neighbours as noise and jump to the next point
        if (seed.size() < minPts) {
            labels[currentIndex] = NOISE;
            for (auto ind: seed) {
                labels[ind] = NOISE;
            }
            continue;
        }
        // start forming a new cluster around the current point, since it is big enough
        cluster = SpeciesFactory::get() -> create();
        labels[currentIndex] = CLUSTERED;
        cluster -> common -> push(entitiesArray[currentIndex]);
        list -> push(std::shared_ptr<Species>(cluster));
        // the set containing all processed elements
        std::unordered_set<unsigned long> neighbourSet;
        for (auto cur: seed) {
            neighbourSet.insert(cur);
        }
        // add the neighbours and their possible neighbours into the cluster recursively
        for (long k = 0; k < seed.size(); ++k) {
            long ind = seed[k];
            // the noise is added to the current cluster
            if (labels[ind] == NOISE) {
                labels[ind] = CLUSTERED;
                cluster -> common -> push(entitiesArray[ind]);
            }
            // ignore all the previously marked point AND the noise that has been
            // assigned a label in the previous if. The neighbours of noise should not be added to cluster
            if (labels[ind] == CLUSTERED) {
                continue;
            }
            // the only option to get here is that a point is unclassified, add it to the current cluster
            labels[ind] = CLUSTERED;
            cluster -> common -> push(entitiesArray[ind]);
            // find neighbours of the current point
            this -> findNeighbours(entitiesArray[ind].get(), entities, &tmpNeighbours); //SPG
            // if the point happened to be a core point, add its neighbours to the seed
            if (tmpNeighbours.size() >= minPts) {
//                seed.insert(seed.end(), tmpNeighbours.begin(), tmpNeighbours.end()); // introduces too many copies
                for (auto cur: tmpNeighbours) {
                    if ((labels[cur])&&(neighbourSet.find(cur) == neighbourSet.end())) {
                        seed.push_back(cur);
                        neighbourSet.insert(cur);  // or "s.emplace(q.back());"
                    }
                }
//                std::cout << "The seed is now: " << seed.size() <<std::endl;
            }
        }
    }
    // create a special noise cluster
    cluster = SpeciesFactory::get() -> create();
    // add the noise to a separate cluster
    for (index = 0; index < labels.size(); ++index) {
        if (labels[index] == NOISE) {
            cluster -> common -> push(entitiesArray[index]);
        }
    }
    // add the species into population only if it has more than one individual
    if (cluster -> common -> getLength() > 0) {
        list -> push(std::shared_ptr<Species>(cluster));
    }
    return list;
}

void DBScan::findNeighbours(Entity *point, List<EntityPtr> *entities, std::vector<unsigned long> *output) {
    Iterator<std::shared_ptr<Entity>> iter(entities);
    Iterator<std::shared_ptr<Entity>> *iter_ptr = &iter;
    Node<EntityPtr> *node;

    // remove the old neighbours if they were present
    output -> clear();

    unsigned long index = 0;
    foreach(node, iter_ptr) {
        // skip the same point
        if (node -> val.get() == point) {
            ++ index;
            continue;
        }
        // add the index of close enough point into an array
        if (this -> metric -> distance(point, node -> val.get()) <= epsilon) {
            output -> push_back(index);
        }
        ++ index;
    }
}

DBScan::DBScan(DistanceMetric *metric, double epsilon, unsigned int minPts) : Clustering(metric) {
    this -> epsilon = epsilon;
    this -> minPts = minPts;
}

double EdgeJaccard::dist(DiscreteEntity *x, DiscreteEntity *y) {
    unsigned long n = x -> chr.size();
    // will contain numbers of cities that are next to the index city in both x's and y's path
//    long neighbours[n][2];
    std::vector<std::vector<long>> &neighbours = neighboursContainer;
    long prev = n - 1, next;
    for (unsigned long i = 0; i < n; ++i) {
        neighbours[x -> chr[i]][0] = x -> chr[prev];
        next = (i + 1) % n;
        neighbours[x -> chr[i]][1] = x -> chr[next];
        prev = i;
    }
    // remove the x's neighbours that are not present in y
    prev = n - 1;
    unsigned long counter = 0;
    for (unsigned long i = 0; i < n; ++i) {
        next = (i + 1) % n;
        // remove the first neighbour of the current city if it is not the neighbour from y's perspective
        if ((neighbours[y -> chr[i]][0] != y->chr[prev]) && (neighbours[y -> chr[i]][0] != y->chr[next])) {
//            neighbours[y -> chr[i]][0] = -1;
            ++ counter;
        }
        // same for the second neighbour
        if ((neighbours[y -> chr[i]][1] != y->chr[prev]) && (neighbours[y -> chr[i]][1] != y->chr[next])) {
//            neighbours[y -> chr[i]][1] = -1;
            ++ counter;
        }
        prev = i;
    }

    // counter contains the number of -1's. The total number of options is 2*n
    // the number of common edges is (2*n - counter)/2, since each edge is counted twice
    unsigned long commonEdges = n - counter/2;
//    std::cout << 1.0 - (double)commonEdges / (double) (2*n - commonEdges) << std::endl;
//    if (uniform() < 0.00001) {
//        std::cout << commonEdges << std::endl;
//    }
    return 1.0 - (double)commonEdges / (double) (2*n - commonEdges);
}

EdgeJaccard::EdgeJaccard(unsigned long chromosome_size) {
    this -> neighboursContainer.resize(chromosome_size);
    for (unsigned long j = 0; j < chromosome_size; ++j) {
        this -> neighboursContainer[j] = std::vector<long>(2);
    }
}

double CachingWrapper::distance(Entity *x, Entity *y) {
    // the distance should be zero if hashes are the same
    if (x -> getHash() == y -> getHash()) {
        return 0.0;
    }
    unsigned long hash = x -> getHash() ^ y -> getHash();
    auto it = this -> map.find(hash);
    ++ queries;

    if (it == this -> map.end()) {
        double rez = this -> metric -> distance(x, y);
        this -> map.insert({hash, rez});
        if (this -> map.size() > 40000000) {
            this -> map.clear();
//            std::cout << "clear!!"<<std::endl;
        }
        return rez;
    }
    ++ hits;
    return it -> second;
}

CachingWrapper::CachingWrapper(DistanceMetric *metric) {
    this -> metric = std::unique_ptr<DistanceMetric>(metric);
}

typedef std::pair<unsigned long, unsigned long> long_pair;

long_pair SpeciesMatcher::deathsAndSplits(Population * first, Population * second) {
    SpeciesNode species, current_species;
    EntityNode node, inner_node;
    double min_dist, dst;
    unsigned long closest_species = 0, i = 0, j;
    // matrix of number of common entities between species
    std::vector<std::vector<unsigned long>> speciesSelected;
    speciesSelected.resize(first -> getLength());
    // maximum distance that allows to say that the entities are close enough to be treated as single one
    bool lonely;
    foreach(species, first) {
        // resize the inner vector to be of the length of the number of species in current population
        speciesSelected[i].resize(second -> getLength());
        // for each individual in each previous species
        foreach (node, species -> val -> common.get()) {
            min_dist = 1.0;
            j = 0;
            // iterate over all the current individuals to find the closest one
            foreach(current_species, second) {
                foreach (inner_node, current_species -> val -> common.get()) {
                    dst = this -> metric -> distance(node -> val.get(), inner_node -> val.get());
                    if (dst < min_dist) {
                        min_dist = dst;
                        closest_species = j;
                    }
                    if (min_dist <= 0.0) {
                        break;
                    }
                }
                // just not to do useless work if the entities are exactly same
                if (min_dist <= 0.0) {
                    break;
                }
                ++ j;

            }
            // check that the closest one is close enough
            if (min_dist < threshold) {
                ++speciesSelected[i][closest_species];
            }
        }
        ++i;
    }
    // now we have the matrix of size n x m, n - number of species in the previous population
    // m - number of species in the current population
    // which is filled by the number of matched individuals
    unsigned long curMax, secondMax, num, died = 0, split = 0;
    species = first -> reset();
    for (auto &row: speciesSelected) {
        // find the number of nonzero entries and the two biggest numbers in the row
        curMax = 0, secondMax = 0, num = 0;
        for (auto commonInds: row) {
            if (commonInds > 0) {
                ++num;
            }
            if (commonInds > curMax) {
                secondMax = curMax;
                curMax = commonInds;
            }
        }
        // if the species is not mapped one to one
        if ((double)curMax / species->val->common->getLength() <= significance_threshold) {
            // check if the species might have been split
            if ((double)(curMax + secondMax) / (double)species->val->common->getLength() > significance_threshold) {
                // the species is split into two different species
                ++ split;
            } else {
                // if not the two of the species
                ++died;
            }
        }
        species = first -> next();
    }
    return {died, split};
}

bool SpeciesMatcher::GoForward(World *world) {
    if (previousPopulation != nullptr) {
        long_pair tmp = this -> deathsAndSplits(previousPopulation.get(), world -> population.get());
        auto died = tmp.first;
        auto split = tmp.second;
        tmp = this -> deathsAndSplits(world -> population.get(), previousPopulation.get());
        auto born = tmp.first;
        auto merged = tmp.second;

        std::cout << "Was " << this -> previousPopulation -> getLength() << " species, died: " << died;
        std::cout << ", split: " << split << ", born " << born << ", merged " << merged << std::endl;
    }
    // store the current population as the previous one
    previousPopulation = move(world -> population);
    world -> population = std::unique_ptr<Population>(new Population(*previousPopulation));
    return true;
}

SpeciesMatcher::SpeciesMatcher(DistanceMetric *metric, double threshold, double significance_threshold) {
    this -> metric = metric;
    this -> threshold = threshold;
    this -> significance_threshold = significance_threshold;
}
