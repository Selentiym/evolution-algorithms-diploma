//
// Created by nikita on 15.02.19.
//

#ifndef GENETIC_ALGORITHM_CPP_CLUSTERING_H
#define GENETIC_ALGORITHM_CPP_CLUSTERING_H

#include "clusteringAbstracts.h"
#include "GeneticAlgorithm.h"
#include <unordered_map>

class DBScan: public Clustering {
public:
    Population * doCluster(EntityList *entities) override;

    DBScan(DistanceMetric *metric, double epsilon, unsigned int minPts);

protected:
    void findNeighbours(Entity *point, EntityList *entities, std::vector<unsigned long> *output);

    double epsilon;

    unsigned int minPts;
};

class EdgeJaccard: public DiscreteDistanceMetric {
public:
    double dist(DiscreteEntity *x, DiscreteEntity *y) override;

    explicit EdgeJaccard(unsigned long chromosome_size);

protected:
    std::vector<std::vector<long>> neighboursContainer;
};

struct pair_hasher {
    template <class T>
    std::size_t operator() (const std::pair<T, T> &pair) const {
        return boost::hash<T>()(pair.first) ^ boost::hash<T>()(pair.second);
    }
};

struct entity_pair_hasher {
    std::size_t operator() (const std::pair<Entity *, Entity *> &pair) const {
        return pair.first -> getHash() ^ pair.second -> getHash();
    }
};

struct fitness_pair_hasher {
    std::size_t operator() (const std::pair<size_t , size_t > &pair) const {
        return pair.first ^ pair.second;
    }
};

class CachingWrapper: public DistanceMetric {
public:
    double distance(Entity *x, Entity *y) override;

    explicit CachingWrapper(DistanceMetric * metric);

    long long hits = 0;
    long long queries = 0;
protected:
    std::unique_ptr<DistanceMetric> metric;
//    std::unordered_map<std::pair<std::vector<unsigned long>,std::vector<unsigned long>>, double, pair_hasher> map;
    std::unordered_map<size_t, double> map;
//    std::unordered_map<std::pair<size_t, size_t>, bool, fitness_pair_hasher> map_bool;
};

class SpeciesMatcher: public Component {
public:
    bool GoForward(World *world) override;

    explicit SpeciesMatcher(DistanceMetric *metric, double threshold, double significance_threshold);
protected:
    PopulationPtr previousPopulation = nullptr;

    DistanceMetric * metric;

    double threshold;

    double significance_threshold;

    typedef std::pair<unsigned long, unsigned long> long_pair;
    long_pair deathsAndSplits(Population * first, Population * second);

};

#endif //GENETIC_ALGORITHM_CPP_CLUSTERING_H
