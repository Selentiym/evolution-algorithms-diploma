//
// Created by nikita on 15.02.19.
//

#include <GeneticAlgorithm.h>
#include "clusteringAbstracts.h"

typedef Iterator<std::shared_ptr<Entity>> iter_t;

bool Clustering::GoForward(World *world) {
    if (world -> population -> getLength() != 1) {
        std::cout << "Performing clustering on a population with more than one species!" << std::endl;
    }
    world -> population = std::unique_ptr<Population>(
        this -> doCluster(world -> population -> head -> val -> common.get())
    );

//    Population *tmp = this -> doCluster(world -> population -> head -> val -> common.get());
//    world -> population ->  return false;
}

Clustering::Clustering(DistanceMetric *metric) {
    this -> metric = metric;
}

//void neighbouringEdges(Entity *x, Entity *y, unsigned long &neighbours) {
//    auto n = x -> chr.size();
//    // store the x's neighbours
//    long prev = n - 1, next;
//    for (unsigned long i = 0; i < n; ++i) {
//        neighbours[i][0] = x -> chr[prev];
//        next = (i + 1) % n;
//        neighbours[i][1] = x -> chr[next];
//        prev = i;
//    }
//    // remove the x's neighbours that are not present in y
//    prev = n - 1;
//    for (unsigned long i = 0; i < n; ++i) {
//        next = (i + 1) % n;
//        // remove the first neighbour of the current city if it is not the neighbour from y's perspective
//        if ((neighbours[i][0] != y->chr[prev]) && (neighbours[i][0] != y->chr[next])) {
//            neighbours[i][0] = -1;
//        }
//        // same for the second neighbour
//        if ((neighbours[i][1] != y->chr[prev]) && (neighbours[i][1] != y->chr[next])) {
//            neighbours[i][1] = -1;
//        }
//    }
//}

double DiscreteDistanceMetric::distance(Entity *x, Entity *y) {
    return this -> dist((DiscreteEntity *) x, (DiscreteEntity *) y);
}
