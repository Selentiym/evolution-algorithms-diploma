//
// Created by nikita on 15.02.19.
//

#ifndef GENETIC_ALGORITHM_CPP_CLUSTERINGABSTRACTS_H
#define GENETIC_ALGORITHM_CPP_CLUSTERINGABSTRACTS_H

#include "GeneticAlgorithm.h"

class DistanceMetric {
public:

    virtual double distance(Entity * x, Entity * y) = 0;
};

class DiscreteDistanceMetric: public DistanceMetric {
public:
    double distance(Entity *x, Entity *y) override;

protected:
    virtual double dist(DiscreteEntity *x, DiscreteEntity *y) = 0;
};

class Clustering: public Component {

public:
    bool GoForward(World *world) override;

    explicit Clustering(DistanceMetric *metric);

    virtual Population * doCluster(EntityList *entities) = 0;

protected:
    DistanceMetric * metric;
};

//class

#endif //GENETIC_ALGORITHM_CPP_CLUSTERINGABSTRACTS_H
