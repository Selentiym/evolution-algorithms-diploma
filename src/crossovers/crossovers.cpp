//
// Created by nikita on 03.02.19.
//

#include <GeneticAlgorithm.h>
#include <set>
#include <cfloat>
#include "crossovers.h"
#include "Helpers.h"

void EdgeRecombinationCrossoverCore::doCross(DiscreteEntity *mom, DiscreteEntity *dad, List<EntityPtr> *outputList) {

    // all the cities are available at first
    auto size = mom -> chr.size();
    bool taken[size];
    for (unsigned long i = 0; i < size; ++i) {
        taken[i] = false;
    }
    // initialize the neighbours of each city
    std::set<unsigned int>neighbours[size];

    //TODO: change type to integer after adopting chromosomes for integer values
    // create aliases just for convenience
    unsigned int *p1, *p2;
    p1 = &(mom ->chr[0]);
    p2 = &(dad ->chr[0]);


    // form neighbourhoods
    neighbours[p1[0]].insert(p1[size-1]);
    neighbours[p2[0]].insert(p2[size-1]);
    neighbours[p1[size-1]].insert(p1[0]);
    neighbours[p2[size-1]].insert(p2[0]);
    for (unsigned long i = 1; i < size; ++i) {
        neighbours[p1[i]].insert(p1[i-1]);
        neighbours[p2[i]].insert(p2[i-1]);
        neighbours[p1[i-1]].insert(p1[i]);
        neighbours[p2[i-1]].insert(p2[i]);
    }

//    Entity *child(EntityFactory::get() -> create());

    auto *child = (DiscreteEntity *)EntityFactory::get() -> create();

    // initialize a child
    unsigned int *ch = &(child -> chr[0]);

    // actions performed when a new city is taken
    std::function<void(unsigned int)> occupyCity = [&] (unsigned int cityNumber) -> void {
        taken[cityNumber] = true;
        for (unsigned int i = 0; i < size; ++i) {
            neighbours[i].erase(cityNumber);
        }
    };

    // fill in the child from one of the parents at random
    if (uniform() < 0.5) {
        ch[0] = p1[0];
    } else {
        ch[0] = p2[0];
    }
    occupyCity(ch[0]);

    std::set<unsigned int>* curSet;
    std::vector<unsigned int> minInds;

    for (unsigned int i = 1; i < size; ++i) {
        curSet = &neighbours[i-1];
        if (not curSet -> empty()) {
            unsigned int curVal, minInd = 0;
            minInds.clear();
            unsigned int curMin = std::numeric_limits<int>::max();
            for(auto candidate: *curSet) {
                curVal = (unsigned int) neighbours[candidate].size();
                if (curVal < curMin) {
                    curMin = curVal;
                    minInds.clear();
                }
                if (curVal == curMin) {
                    minInds.push_back(candidate);
                }
            }
            minInd = minInds[getRandInt(minInds.size())];
            occupyCity(minInd);
            ch[i] = minInd;
        } else {
            // choose one of not visited cities at random
//            gene toChoose = (gene)getRandInt(size - i) + 1;
            auto toChoose = (unsigned int) getRandInt(size - i);
            for (unsigned int j = 0; j < size; ++j) {
                if (taken[j]) {
                    continue;
                }
                toChoose--;
                if (toChoose == 0) {
                    // select the element j
                    occupyCity(j);
                    ch[i] = j;
                    break;
                }
            }
        }
    }
//    double tmp111;
//    tmp111 = uniform();
//    tmp111 += 1;
    outputList -> push(std::shared_ptr<Entity>(child));
}

void DistancePreservingCrossoverCore::doCross(DiscreteEntity *mom, DiscreteEntity *dad, List<EntityPtr> *outputList) {
    unsigned long n = mom -> chr.size();
    // will contain numbers of cities that are next to the index city in the parents' path
    long neighbours[n][2];
    // store the mom's neighbours
    long prev = n - 1, next;
    for (unsigned long i = 0; i < n; ++i) {
        neighbours[mom -> chr[i]][0] = mom -> chr[prev];
        next = (i + 1) % n;
        neighbours[mom -> chr[i]][1] = mom -> chr[next];
        prev = i;
    }
    // remove the mom's neighbours that are not present in dad
    prev = n - 1;
    unsigned long i;
    for (unsigned long j = 0; j < n; ++j) {
        next = (j + 1) % n;
        i = dad->chr[j];
        // remove the first neighbour of the current city if it is not the neighbour from dad's perspective
        if ((neighbours[i][0] != dad->chr[prev]) && (neighbours[i][0] != dad->chr[next])) {
            neighbours[i][0] = -1;
        }
        // same for the second neighbour
        if ((neighbours[i][1] != dad->chr[prev]) && (neighbours[i][1] != dad->chr[next])) {
            neighbours[i][1] = -1;
        }
        prev = j;
    }
    // count the number of neighbours that mom and dad agree on for each city
    // and form vectors for single neighboured and double neighboured cities to use later

    bool endings[n];
    bool visited[n];
    long curCity = 0;
    unsigned long endingsCount = 0;
    for (unsigned long i = 0; i < n; ++i) {
        visited[i] = false;
        // the city is not an ending only if both neighbours are present
        if ((neighbours[i][0] != -1) && (neighbours[i][1] != -1)) {
            endings[i] = false;
        } else {
            endings[i] = true;
            // remember the number of endings to select from them later
            ++ endingsCount;
        }
    }

    // sample one ending index at random
    // this will be the ending to start with
    unsigned long to_select = getRandInt(endingsCount);

    // find the corresponding ending by its index
    for (unsigned long j = 0; j < n; ++j) {
        if (endings[j]) {
            -- to_select;
        }
        if (to_select < 0) {
            curCity = j;
            break;
        }
    }

    auto * child = (DiscreteEntity *)EntityFactory::get() -> create();

    outputList -> push(std::shared_ptr<Entity>(child));

    visited[curCity] = true; //don't forget to mark the first city as visited
    // now take any ending as a current city and go to its neighbours till it is possible
    // when the current city has no unvisited neighbours select the
    // closest city from the unvisited endings and start all over

    for (unsigned long j = 0; j < n; ++j) {
        // mark that the current city should not be used
        visited[curCity] = true;
        endings[curCity] = false;
        // add the current city to the child
        child -> chr[j] = (unsigned int)curCity;
        // try to jump to the first neighbour
        if ((neighbours[curCity][0] != -1) && (not visited[neighbours[curCity][0]])) {
            curCity = neighbours[curCity][0];
            continue;
        }
        // try to jump to the second neighbour
        if ((neighbours[curCity][1] != -1) && (not visited[neighbours[curCity][1]])) {
            curCity = neighbours[curCity][1];
            continue;
        }
        // select the closest ending that is not visited yet if both neighbours are either
        // visited or not present
        auto min = DBL_MAX;
        unsigned int argMin = 0;
        for (unsigned long k = 0; k < n; ++k) {
            if (not endings[k]) {
                continue;
            }
            if (min > (*distances)[curCity][k]) {
                min = (*distances)[curCity][k];
                argMin = (unsigned int)k;
            }
        }
        // declare the closest ending as the current city
        curCity = argMin;
    }
    // just checking that nothing is broken..
//    if (someGenesMissing(outputList -> tail -> val.get())) {
//        std::cout << "Missing Genes found!!!!" << std::endl;
//    }
}

DistancePreservingCrossoverCore::DistancePreservingCrossoverCore(matrix<float> *m) {
    distances = m;
}

void RotationalOrderedCrossoverCore::doCross(DiscreteEntity *mom, DiscreteEntity *dad, List<EntityPtr> *outputList) {

    auto size = (discrete_gene)mom -> chr.size();

    // choose the starting and ending points for the mutation
    discrete_gene start = getRandInt(size - 1); // intentially removed one to leave some place for the second index
    discrete_gene end = getRandInt(size - 1, start + 1); // intentially removed one to solve the problems with boundaries

    // create and output an offspring
    auto child = (DiscreteEntity *)EntityFactory::get() -> create();
    outputList -> push(EntityPtr(child));

    std::vector<bool> occupied(size, false);

    size_t i;
    // copy the chunk of parent that is going to be unchanged
    for (i = start; i < end; ++i) {
        child -> chr[i] = mom -> chr[i];

        occupied[mom -> chr[i]] = true;
    }

    size_t remaining = size - end + start;

    // collect the genes that are absent from the offspring yet
    std::vector<discrete_gene> to_insert;
    to_insert.reserve(remaining);
    for (size_t _ = 0; _ < size; ++_) {
        ++i;
        i = i % size;
        if (not occupied[dad -> chr[i]]) {
            to_insert.push_back(dad -> chr[i]);
        }
    }

    // find the optimal starting point among the remaining cities
    discrete_gene left = child -> chr[start], right = child -> chr[end];
//    size_t argMin = 0;
//    double min = (*m)[to_insert[remaining-1]][left] + (*m)[to_insert[0]][right], cur;
//    for (i = 1; i < remaining; ++i) {
//        cur = (*m)[to_insert[i-1]][left] + (*m)[to_insert[i]][right];
//    }
    discrete_gene prev = to_insert[remaining - 1];
    double min, cur;
    long argMin = -1;
    for (i = 0; i < remaining; ++i) {
        cur = (*m)[prev][left] + (*m)[to_insert[i]][right];
        prev = to_insert[i];
        if ((argMin == -1) || (cur < min)) {
            argMin = i;
            min = cur;
        }
    }

    auto ind_remaining = (size_t)argMin, ind_insert = (size_t)end % size;
    for (i = 0; i < remaining; ++i) {
        child -> chr[ind_insert] = to_insert[ind_remaining];
        ++ind_remaining;
        ++ind_insert;
        ind_remaining %= remaining;
        ind_insert %= size;
    }

    //TODO: copy the remaining genes starting from argMin
}

RotationalOrderedCrossoverCore::RotationalOrderedCrossoverCore(matrix<float> *m) {
    this -> m = m;
}
