//
// Created by nikita on 03.02.19.
//

#ifndef GENETIC_ALGORITHM_CPP_CROSSOVERS_H
#define GENETIC_ALGORITHM_CPP_CROSSOVERS_H

#include "GeneticAlgorithm.h"
#include "crossoversAbstracts.h"

class EdgeRecombinationCrossoverCore: public DiscreteCrossoverCore {
public:
    void doCross(DiscreteEntity *mom, DiscreteEntity *dad, EntityList *outputList);
};

class DistancePreservingCrossoverCore: public DiscreteCrossoverCore {
public:
    void doCross(DiscreteEntity *mom, DiscreteEntity *dad, EntityList *outputList);

    explicit DistancePreservingCrossoverCore(matrix<float> *m);

protected:
    matrix<float> *distances;
};

class RotationalOrderedCrossoverCore: public DiscreteCrossoverCore {
public:
    void doCross(DiscreteEntity *mom, DiscreteEntity *dad, List<EntityPtr> *outputList);

    explicit RotationalOrderedCrossoverCore(matrix<float> *m);

protected:
    matrix<float> *m;
};

#endif //GENETIC_ALGORITHM_CPP_CROSSOVERS_H
