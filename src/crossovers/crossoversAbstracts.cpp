//
// Created by nikita on 03.02.19.
//

#include <memory>
#include <GeneticAlgorithm.h>
#include "crossoversAbstracts.h"
#include "Helpers.h"


CoredCrossover::CoredCrossover(DiscreteCrossoverCore *core) {
    this -> core = core;
}

bool RollOfFortuneCrossover::speciesForward(Species *species, World *world) {
    // Only one individual is born at the moment
    std::vector<double> fitnesses;
    extractFitnessesFromEntityList(fitnesses, species->common.get(), false);
    if (this -> transformer != nullptr) {
        this -> transformer -> transform(fitnesses);
    }
    normalizeFitnesses(fitnesses);
    for (size_t _ = 0; _ < this -> num; ++_) {
        Entity *p1 = nullptr, *p2 = nullptr;
        double u1 = uniform(), u2 = uniform();
        EntityNode node;
        double tmpSum = 0;
        size_t counter = 0;
        foreach(node, species->common.get()) {
            tmpSum += fitnesses[counter];
            if ((tmpSum > u1) && (p1 == nullptr)) {
                p1 = node->val.get();
            }
            if ((tmpSum > u2) && (p2 == nullptr)) {
                p2 = node->val.get();
            }
            ++counter;
        }
        this->core->doCross(p1, p2, species->crossovered.get());
    }
    return true;
}

RollOfFortuneCrossover::RollOfFortuneCrossover(DiscreteCrossoverCore *core, size_t num, FitnessTransformer *transformer)
        : CoredCrossover(core) {
    this -> transformer = std::unique_ptr<FitnessTransformer>(transformer);
    this -> num = num;
}

bool ProbabilisticCrossover::speciesForward(Species *species, World *world) {
    //
    EntityNode node1, node2;
//    std::cout << species -> common -> getLength() << '\n';
    typedef Iterator<std::shared_ptr<Entity>> iter_t;
    iter_t iter(species -> common.get());
    iter_t *iter_ptr = &iter;
    unsigned long i = 0, j;
    foreach(node1, species -> common.get()) {
        ++i;
        j = 0;
        foreach(node2, iter_ptr) {
            ++j;
            if (i >= j) {
                continue;
            }
            if (probaTrial(this -> proba)) {
                this -> core -> doCross(node1 -> val.get(), node2 -> val.get(), species -> crossovered.get()); //SPG
            }
        }
    }
    return true;
}

ProbabilisticCrossover::ProbabilisticCrossover(DiscreteCrossoverCore *core, double proba) : CoredCrossover(core) {
    this -> proba = proba;
}

void DiscreteCrossoverCore::doCross(Entity *mom, Entity *dad, List<EntityPtr> *outputList) {
    this -> doCross((DiscreteEntity *) mom, (DiscreteEntity *) dad, outputList);
}

FixedNumberCrossover::FixedNumberCrossover(DiscreteCrossoverCore *core, unsigned long num,
                                           unsigned long population_size) : CoredCrossover(core) {
    this -> num = num;
    this -> population_size = population_size;
}

bool FixedNumberCrossover::speciesForward(Species *species, World *world) {
    EntityNode node1, node2;
    typedef Iterator<std::shared_ptr<Entity>> iter_t;
    iter_t iter(species -> common.get());
    iter_t *iter_ptr = &iter;
    // total size of current species
    auto n = species -> common -> getLength();
    // number of offspring created so far
    unsigned long hits = 0;
    // number of offsprings we are to create in this species
    // proportional to the initial size of the species
    unsigned long max_num;

    max_num = (unsigned long)round(((double)n / this -> population_size) * (this -> num + this -> population_size));

    double effective_proba = (double)max_num * 1.1/(n*n);

    bool stop_iter = false;
    foreach(node1, species -> common.get()) {
        foreach(node2, iter_ptr) {
            if (probaTrial(effective_proba)) {
                this -> core -> doCross(node1 -> val.get(), node2 -> val.get(), species -> crossovered.get()); //SPG
                ++ hits;
                if (hits > max_num) {
                    stop_iter = true;
                    break;
                }
            }
        }
        if (stop_iter) {
            break;
        }
    }
    return true;
}
