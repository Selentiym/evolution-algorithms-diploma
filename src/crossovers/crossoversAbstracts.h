//
// Created by nikita on 03.02.19.
//

#ifndef GENETIC_ALGORITHM_CPP_CROSSOVERSABSTRACTS_H
#define GENETIC_ALGORITHM_CPP_CROSSOVERSABSTRACTS_H

#include <vector>
#include <iostream>
#include <boost/shared_ptr.hpp>
#include "../../src/list/List.h"
#include "Helpers.h"
#include "GeneticAlgorithm.h"

class DiscreteCrossoverCore {
public:
    void doCross(Entity *mom, Entity *dad, List<EntityPtr> *outputList);

    virtual void doCross(DiscreteEntity *mom, DiscreteEntity *dad, List<EntityPtr> *outputList) = 0;
};

class CoredCrossover: virtual public Component {
public:
    explicit CoredCrossover(DiscreteCrossoverCore *core);

protected:
    DiscreteCrossoverCore* core;
};

class RollOfFortuneCrossover: public CoredCrossover, public SpeciesWiseComponent {
public:
    explicit RollOfFortuneCrossover(DiscreteCrossoverCore *core, size_t num, FitnessTransformer *transformer = nullptr);

    bool speciesForward(Species *species, World *world) override;

protected:
    std::unique_ptr<FitnessTransformer> transformer;
    size_t num;
};

class ProbabilisticCrossover: public CoredCrossover, public SpeciesWiseComponent {
public:
    explicit ProbabilisticCrossover(DiscreteCrossoverCore *core, double proba);

    bool speciesForward(Species *species, World *world) override;

protected:
    double proba;
};

class FixedNumberCrossover: public CoredCrossover, public SpeciesWiseComponent {
public:
    explicit FixedNumberCrossover(DiscreteCrossoverCore *core, unsigned long num,
                                      unsigned long population_size);

    bool speciesForward(Species *species, World *world) override;

protected:
    unsigned long num, population_size;
};

#endif //GENETIC_ALGORITHM_CPP_CROSSOVERSABSTRACTS_H
