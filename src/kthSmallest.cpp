//
// Created by nikita on 23.01.19.
//


// C++ implementation of worst case linear time algorithm
// to find k'th smallest element
#include<iostream>
#include<algorithm>
#include<climits>
#include "Helpers.h"

using namespace std;

ind_helpers partition(double arr[], ind_helpers l, ind_helpers r, double x);

// A simple function to find median of arr[].  This is called
// only for an array of size 5 in this program.
double findMedian(double arr[], ind_helpers n)
{
    sort(arr, arr+n);  // Sort the array
    return arr[n/2];   // Return middle element
}

// Returns k'th smallest element in arr[l..r] in worst case
// linear time. ASSUMPTION: ALL ELEMENTS IN ARR[] ARE DISTINCT
double kthSmallest_inner(double arr[], ind_helpers l, ind_helpers r, ind_helpers k)
{
    // If k is smaller than number of elements in array
    if (k > 0 && k <= r - l + 1)
    {
        ind_helpers n = r-l+1; // Number of elements in arr[l..r]

        // Divide arr[] in groups of size 5, calculate median
        // of every group and store it in median[] array.
        ind_helpers i;
        double median[(n+4)/5]; // There will be floor((n+4)/5) groups;
        for (i=0; i<n/5; i++)
            median[i] = findMedian(arr+l+i*5, 5);
        if (i*5 < n) //For last group with less than 5 elements
        {
            median[i] = findMedian(arr+l+i*5, n%5);
            i++;
        }

        // Find median of all medians using recursive call.
        // If median[] has only one element, then no need
        // of recursive call
        double medOfMed = (i == 1)? median[i-1]:
                       kthSmallest_inner(median, 0, i-1, i/2);

        // Partition the array around a random element and
        // get position of pivot element in sorted array
        ind_helpers pos = partition(arr, l, r, medOfMed);

        // If position is same as k
        if (pos-l == k-1)
            return arr[pos];
        if (pos-l > k-1)  // If position is more, recur for left
            return kthSmallest_inner(arr, l, pos-1, k);

        // Else recur for right subarray
        return kthSmallest_inner(arr, pos+1, r, k-pos+l-1);
    }

    // If k is more than number of elements in array
    return INT_MAX;
}

void swap(double *a, double *b)
{
    double temp = *a;
    *a = *b;
    *b = temp;
}

// It searches for x in arr[l..r], and partitions the array
// around x.
ind_helpers partition(double arr[], ind_helpers l, ind_helpers r, double x)
{
    // Search for x in arr[l..r] and move it to end
    ind_helpers i;
    for (i=l; i<r; i++)
        if (arr[i] == x)
            break;
    swap(&arr[i], &arr[r]);

    // Standard partition algorithm
    i = l;
    if (l < r) {
        for (ind_helpers j = l; j <= r - 1; j++)
        {
            if (arr[j] <= x)
            {
                swap(&arr[i], &arr[j]);
                i++;
            }
        }
        swap(&arr[i], &arr[r]);
    }
    return i;
}

double kthLargest(std::vector<double> &v, ind_helpers k) {
    // {10,20,30,40,50}
    // [0, 1, 2, 3, 4]
    // size == 5
    // want 0 largest (biggest), that would be 5 - 0 - 1 = 4 smallest
    // want 2 largest, 5 - 2 - 1 = 2 smallest
    // want 4 largest, 5 - 4 - 1 = 0 smallest

    return kthSmallest(v, v.size() - k - 1);
}

double kthLargest_sort(std::vector<double> &v, ind_helpers k) {
    // {10,20,30,40,50}
    // [0, 1, 2, 3, 4]
    // size == 5
    // want 0 largest (biggest), that would be 5 - 0 - 1 = 4 smallest
    // want 2 largest, 5 - 2 - 1 = 2 smallest
    // want 4 largest, 5 - 4 - 1 = 0 smallest

    sort(v.begin(), v.end());
    return v[v.size() - k - 1];
}

double kthSmallest_sort(std::vector<double> &v, ind_helpers k) {
    sort(v.begin(), v.end());
    return v[k];
}

double kthSmallest(std::vector<double> &v, ind_helpers k) {
    double *tmp = &v[0];
    ind_helpers sz = v.size();
    return kthSmallest_inner(tmp, 0, sz - 1, k + 1);
};
