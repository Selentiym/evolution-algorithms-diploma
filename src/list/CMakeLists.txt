cmake_minimum_required(VERSION 3.12)
project(list_tests)

set(CMAKE_CXX_STANDARD 11)


add_executable(list_tests tests.h List.h)