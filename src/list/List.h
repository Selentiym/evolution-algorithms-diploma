//
// Created by nikita on 02.01.19.
//

#ifndef GENETIC_ALGORITHM_CPP_LIST_H
#define GENETIC_ALGORITHM_CPP_LIST_H

template <typename T>
class List;


template <typename T>
class Node {
public:
    T val;
    Node<T> * next = nullptr;
    Node<T> * prev = nullptr;

    explicit Node(T valvar) {
        val = valvar;
    }
};


template <typename T>
class Iterator {
public:
    explicit Iterator(List<T> * list);
    Node<T> * next();

    Node<T> * reset();

protected:
    Node<T> * current = nullptr;
    Node<T> * head = nullptr;
};


template <typename T>
class List {
protected:
    unsigned long length = 0;
public:
    Node<T> * head = nullptr;
    Node<T> * tail = nullptr;
    Node<T> * current = nullptr;

    List() = default;

    void push(T val);

    Node<T> * next();

    unsigned long getLength();

    void removeNode(Node<T> * node);

    void addValuesFrom(List<T>* valueSource);


    /**
     * The list that is used as a valueSource remains intact,
     * new Node objects are created. O(<length of value source>)
     * @param transferSource
     */
    void transferValuesFrom(List<T>* transferSource);


    /**
     * The list that is transferred from will become empty
     * after the operation. No new Node objects are created.
     * Works in O(1)
     */
    virtual ~List();

    List(List<T> &source);

    Node<T> * reset();
};

#define foreach(var, list) for (var = list -> reset(); var = list -> next();)
#define foreachval(var, list) for (var = list -> head -> val; var = list -> next() -> val;)

#include "List.tpp"

#endif //GENETIC_ALGORITHM_CPP_LIST_H
