//
// Created by nikita on 02.01.19.
//


#include "List.h"


template<typename T>
void List<T>::push(T val) {
    auto * valNode = new Node<T>(val);
    if (tail == nullptr) {
        // the list is empty, so just let the current element be the head and tail
        tail = valNode;
        head = valNode;
    } else {
        // save the previous element
        Node<T> * prev = tail;
        // add the new element to the end of the list
        prev -> next = valNode;
        // update the pointer to the last element
        tail = valNode;
        // save the pointer to the previous element
        valNode -> prev = prev;
    }
    ++length;
}

template<typename T>
Node<T> *List<T>::next() {
    if (current == nullptr) {
        current = head;
    } else {
        current = current -> next;
    }
    return current;
}

template<typename T>
void List<T>::addValuesFrom(List<T> *valueSource) {
    /**
     * Adds values from the given list to the current one
     * Recreates all the Node objects and leaves the old list intact
     */
    Node<T> * cur;
    foreach(cur, valueSource) {
        this -> push(cur-> val);
    }
}

template<typename T>
List<T>::~List() {
    Node<T> * toDel;
    Node<T> * nextStep;
    // the list should be not empty
    if (this -> head != nullptr) {
        nextStep = this -> head;
        while (true) {
            toDel = nextStep;
            // save the link to the next node in advance, before deleting the current node that does hold the pointer
            nextStep = toDel -> next;
            delete toDel;
            if (nextStep == nullptr) {
                break;
            }
        }
    }
}

template<typename T>
unsigned long List<T>::getLength() {
    return length;
}

template<typename T>
void List<T>::transferValuesFrom(List<T> *transferSource) {

    // there is nothing to do if the second list is empty
    if (transferSource -> getLength() > 0) {
        // head of second list is now after the tail of the first
        // works fine even if the first list is empty
        transferSource -> head -> prev = this -> tail;
        // if the first list is not empty
        if (this -> getLength() > 0) {
            // tail of the first list is now before the head of the second
            this -> tail -> next = transferSource->head;
        } else {
            // just replace the head of the first list by the head of the second
            this -> head = transferSource -> head;
        }
        // remove the head of the second list so that the stolen Nodes were not destroyed
        // when deleting the second list
        transferSource -> head = nullptr;

        // update the tail of the first list
        // works fine even if the first list is empty
        this -> tail = transferSource -> tail;

        // remove the tail of the second  list to make it absolutely empty
        transferSource -> tail = nullptr;

        //increment the length
        this -> length += transferSource -> getLength();

        //put the length to zero
        transferSource -> length = 0;
    }
    // remove the second list
//    delete transferSource;
}

template<typename T>
List<T>::List(List<T> &source) {
    Node<T> *node;
    List<T> *tmp = &source;
    foreach(node, tmp) {
        this -> push(node -> val);
    }
}

template<typename T>
void List<T>::removeNode(Node<T> *node) {
    // in order not to break iteration over the list if applicable
    if (current == node) {
        current = node -> prev;
    }
    // update head if the head is being deleted
    if (node == this -> head) {
        this -> head = node -> next;
    } else {
        // or just fix the link
        node -> prev -> next = node -> next;
    }
    // update tail if the tail is being deleted
    if (node == this -> tail) {
        this -> tail = node -> prev;
    } else {
        node -> next -> prev = node -> prev;
    }
    // do not forget to decrement the length of the list!
    --this -> length;
    delete node;
}

template<typename T>
Node<T> *List<T>::reset() {
    current = nullptr;
    return head;
}

template<typename T>
Node<T> *Iterator<T>::next() {
    if (current == nullptr) {
        current = head;
    } else {
        current = current -> next;
    }
    return current;
}

template<typename T>
Iterator<T>::Iterator(List<T> *list) {
    current = nullptr;
    head = list -> head;
}

template<typename T>
Node<T> *Iterator<T>::reset() {
    current = nullptr;
    return head;
}
