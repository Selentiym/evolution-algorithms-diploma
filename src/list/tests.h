//
// Created by nikita on 11.01.19.
//

#include <iostream>
#include "List.h"

//int runListTests() {
//    auto lst = new List<int>();
//    auto lst2 = new List<int>();
//    lst->push(11);
//    lst->push(12);
//    lst->push(13);
//    lst2->push(21);
//    lst2->push(22);
//    lst2->push(23);
//    lst->transferValuesFrom(lst2);
//    Node<int> * tmp;
//    foreach(tmp, lst) {
//        std::cout << tmp -> val << std::endl;
//    }
//    std::cout << lst2 -> head;
//
////    auto tmp1 = new ProbabilisticMutation();
////    delete tmp1;
//
//    return 0;
//}

bool testFirstEmpty() {
    auto lst = new List<int>();
    auto lst2 = new List<int>();
    lst2->push(21);
    lst2->push(22);
    lst2->push(23);
    lst->transferValuesFrom(lst2);
    if (lst -> getLength() != 3) {
        std::cout << "incorrect length of the resulting list";
        throw std::exception();
    }
    if (lst2 -> getLength() != 0) {
        std::cout << "incorrect length of second list";
        throw std::exception();
    }
    int arr[3] = {21,22,23};
    int num = 0;
    Node<int> * tmp;
    foreach(tmp, lst) {
        if (tmp -> val != arr[num]) {
            std::cout << "incorrect value "<< tmp -> val << " instead of " << arr[num] << " on " << num << " iteration";
            return false;
            //            throw std::exception();
        }
        ++num;
    }
    return true;
}


int runListTests() {
    return (int) not testFirstEmpty();
}

