//
// Created by nikita on 20.03.19.
//

#include "loggers.h"
#include "Helpers.h"
//#include "stdio.h"

bool ProbabilisticSpeciesWiseLogger::GoForward(World *world) {
//    if (probaTrial(this -> proba)) {
//        return SpeciesWiseComponent::GoForward(world);
//    } else {
//        return true;
//    }
    throw "ProbabilisticSpeciesWiseLogger::GoForward not implemented";
}

ProbabilisticSpeciesWiseLogger::ProbabilisticSpeciesWiseLogger(LoggerCore *core, double proba): CoredLogger(core) {
    this -> proba = proba;
}

void IndividualsLogger::beforeLoop(Species *species, World *world) {
    std::cout << "species" <<std::endl;
}


CoredLogger::CoredLogger(LoggerCore *core) {
    this -> core = core;
}

GenerationalSpeciesWiseLogger::GenerationalSpeciesWiseLogger(LoggerCore *core, std::vector<unsigned long> generations)
        : CoredLogger(core) {
    this -> generations = generations;
}

bool GenerationalSpeciesWiseLogger::GoForward(World *world) {
    auto iter = (unsigned long)world -> getIteration();
    for (auto el: this -> generations) {
        if (iter == el) {
            return SpeciesWiseComponent::GoForward(world);
        }
    }
//    if (this -> generations -> find((unsigned long)world -> getIteration()) != this -> generations -> end()) {
//        return SpeciesWiseComponent::GoForward(world);
//    }
    return true;
}

bool GenerationalSpeciesWiseLogger::speciesForward(Species *species, World *world) {
    Node<EntityPtr> *entity;
    this -> core -> beforeLoop(species, world);
    foreach(entity, species -> common) {
        this -> core -> go(entity -> val.get());
    }
}

void LoggerCore::beforeLoop(Species *species, World *world) {
//    return;  // do nothing
}

void IndividualsLogger::go(Entity *entity) {
    printIndividual((DiscreteEntity *) entity);
}

EachGenerationSpeciesWiseLogger::EachGenerationSpeciesWiseLogger(LoggerCore *core)
        : CoredLogger(core) {

}

//bool EachGenerationSpeciesWiseLogger::GoForward(World *world) {
//    return SpeciesWiseComponent::GoForward(world);
//}
//
// TODO: refactor - move this basic speciesForward into a higher abstraction over
// this class and GenerationalSpecieswiseLogger, since they share the same exact code
bool EachGenerationSpeciesWiseLogger::speciesForward(Species *species, World *world) {
    Node<EntityPtr> *entity;
    this -> core -> beforeLoop(species, world);
    unsigned long hash;
    std::set<unsigned long> hashes;
    foreach(entity, species -> common) {
        hash = entity -> val -> getHash();
        if (hashes.find(hash) == hashes.end()) {
            this -> core -> go(entity -> val.get());
//            std::cout << hash << '\n';
            hashes.insert(hash);
        }
    }
    return true;
}
