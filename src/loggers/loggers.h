//
// Created by nikita on 20.03.19.
//

#ifndef GENETIC_ALGORITHM_CPP_LOGGERS_H
#define GENETIC_ALGORITHM_CPP_LOGGERS_H


#include <GeneticAlgorithm.h>
#include <set>

class LoggerCore {
public:
    virtual void go(Entity *entity) = 0;

    virtual void beforeLoop(Species *species, World *world);
};

class CoredLogger {
public:
    explicit CoredLogger(LoggerCore * core);

protected:
    LoggerCore *core;
};

class ProbabilisticSpeciesWiseLogger: public SpeciesWiseComponent, public CoredLogger {
public:
//    explicit ProbabilisticSpeciesWiseLogger(double proba);
    explicit ProbabilisticSpeciesWiseLogger(LoggerCore *core, double proba);

    bool GoForward(World *world) override;

protected:
    double proba;
};

class GenerationalSpeciesWiseLogger: public SpeciesWiseComponent, public CoredLogger {
public:
    GenerationalSpeciesWiseLogger(LoggerCore *core, std::vector<unsigned long> generations);

    bool GoForward(World *world) override;

    bool speciesForward(Species *species, World *world) override;

protected:
    std::vector<unsigned long> generations;
};


class EachGenerationSpeciesWiseLogger: public SpeciesWiseComponent, public CoredLogger {
public:
    explicit EachGenerationSpeciesWiseLogger(LoggerCore *core);

//    bool GoForward(World *world) override;
//
    bool speciesForward(Species *species, World *world) override;

};

class IndividualsLogger: public LoggerCore {
public:
    void go(Entity *entity) override;

    void beforeLoop(Species *species, World *world) override;
};

#endif //GENETIC_ALGORITHM_CPP_LOGGERS_H
