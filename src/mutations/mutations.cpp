//
// Created by nikita on 22.01.19.
//

#include "mutations.h"

Entity *DMMutation::doMutate(DiscreteEntity *parent, DiscreteEntity *child) {
    //TODO: optimize the solution
    typedef int chr_val;
    auto size = (chr_val)child -> chr.size();
    chr_val tmp[size];

    // choose the starting and ending points for the mutation
    chr_val start = getRandInt(0, size - 1);
    chr_val end = getRandInt(start, size - 1);
    // choose the point where to insert the fragment (after the fragment has been deleted)
    chr_val to_insert = getRandInt(0, size - (end - start) - 1);

    // the shift of indices of the elements of the fragment
    long int shift = to_insert - start;

    // now I need to move the subtour start-end so that its new
    // starting position was to_insert and all the other cities just shifted

    // move the fragment
    for (chr_val i = start; i < end; ++i) {
        tmp[i + shift] = parent -> chr[i];
        child -> chr[i] = -1; // mark the used genes
    }
    // now the old array contains only the digits missing from the new one and -1
    // 1 2 3 -1 -1 -1 7 8  - the old array
    // * 4 5 6 * * * *
    chr_val j = 0;
    for (chr_val i = 0; i < size; ++i) {
        if (child -> chr[j] == -1) {
            // jump to the place in the old array that contains not used values
            j += end - start;
        }
        if ((i >= to_insert) && (i < to_insert + end - start)) {
            continue; // skip the filled values
        }
        tmp[i] = parent -> chr[j];
        ++j;
    }
    // copy the result
    for (chr_val i = 0; i < size; ++i) {
        child -> chr[i] = tmp[i];
    }
    return child;
}


//Entity *DMMutation::doMutate(Entity *parent, Entity *child) {
//    // Only for discrete optimization
//    typedef int chr_val;
//    auto size = (chr_val)child -> chr.size();
//
//    // choose the starting and ending points for the mutation
//    auto start = getRandInt<chr_val>(size - 1);
//    auto end = getRandInt<chr_val>(size - 1);
//    // choose the point where to insert the fragment (after the fragment has been deleted)
//    chr_val to_insert = getRandInt(size - 1);
//
//    // the shift of indices of the elements of the fragment
//    chr_val shift = to_insert - start;
//
//    // now I need to move the subtour start-end so that its new
//    // starting position was to_insert and all the other cities just shifted
//
//    // move the fragment
//    chr_val i;
//    for (i = start; i < end; ++i) {
//        child -> chr[i + shift] = parent -> chr[i];
//    }
//    // 1 2 3 * * * 7 8  - the old array
//    // * 4 5 6 * * * * - the new array
//    chr_val j = 0;
//    for (i = 0; i < size; ++i) {
//        if (i == start) {
//            // jump to the place in the old array that contains not used values
//            j += end - start;
//        }
//        if ((i >= to_insert) && (i < to_insert + end - start)) {
//            continue; // skip the filled values
//        }
//        child -> chr[i] = parent -> chr[j];
//        ++j;
//    }
//    return child;
//}
Entity *SimpleInversionMutationCore::doMutate(DiscreteEntity *parent, DiscreteEntity *child) {
    size_t n = parent -> chr.size();
    // draw start and end of the inversed segment
    auto start = getRandInt(n - 1);
    auto stop = getRandInt(n - 1, start + 1);
    for (size_t i = 0; i < n; ++i) {
        // if we are out of the inverted segment, just copy the gene
        if ((i < start) or (i > stop)) {
            child -> chr[i] = parent -> chr[i];
        } else {
            // else reverse the order
            child -> chr[stop + start - i] = parent -> chr[i];
        }
    }
    return child;
}
