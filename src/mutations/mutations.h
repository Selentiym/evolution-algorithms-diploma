//
// Created by nikita on 22.01.19.
//

#ifndef GENETIC_ALGORITHM_CPP_DMMUTATION_H
#define GENETIC_ALGORITHM_CPP_DMMUTATION_H

#include "GeneticAlgorithm.h"
#include "Helpers.h"
#include "mutationsAbstracts.h"


class DMMutation: public DiscreteMutationCore {
public:
    Entity *doMutate(DiscreteEntity *parent, DiscreteEntity *child) override;
};

class SimpleInversionMutationCore: public DiscreteMutationCore {
public:
    Entity *doMutate(DiscreteEntity *parent, DiscreteEntity *child);
};

typedef SimpleInversionMutationCore SIMMutation;

#endif //GENETIC_ALGORITHM_CPP_DMMUTATION_H
