//
// Created by nikita on 03.02.19.
//

#include "mutationsAbstracts.h"

/**********************************************
 *                  Mutation
 **********************************************/

Mutation::Mutation(MutationCore *mutationCore, bool replaceParents) {
    replace = replaceParents;
    core = std::unique_ptr<MutationCore>(mutationCore);
}

void Mutation::storeMutationResult(Node<EntityPtr> *parentNode, Entity *newEntity, Species *species) {
    //TODO: add a decision based on a callable
    if (replace) {
        parentNode -> val = std::shared_ptr<Entity>(newEntity);
    } else {
        species -> mutated -> push(std::shared_ptr<Entity>(newEntity));
    }
}

ProbabilisticMutation::ProbabilisticMutation(MutationCore *mutationCore, bool replaceParents, double probability)
: Mutation(mutationCore, replaceParents) {
    proba = probability;
}

bool ProbabilisticMutation::speciesForward(Species *species, World *world) {
    Node<EntityPtr> *entity;
    foreach(entity, species -> common) {
        if (probaTrial(this -> proba)) {
            auto newEntity = EntityFactory::get()->create();
            this -> core -> mutate(entity -> val.get(), newEntity);  //SPG
            this -> storeMutationResult(entity, newEntity, species);
        }
    }
    return true;
}

bool SingleMutation::speciesForward(Species *species, World *world) {
    int ind = 0;
    int toMutate = (int) getRandInt(species -> common -> getLength());
    Node<EntityPtr> *node;
    foreach(node, species -> common) {
        if (ind == toMutate) {
            Entity *newEntity = EntityFactory::get() -> create();
            this -> core -> mutate(node -> val.get(), newEntity);
            this -> storeMutationResult(node, newEntity, species);
        }
        ++ ind;
    }
    return true;
}

SingleMutation::SingleMutation(MutationCore *mutationCore, bool replaceParents) : Mutation(mutationCore,
                                                                                           replaceParents) {

}

Entity *DiscreteMutationCore::mutate(Entity *parent, Entity *child) {
    return this -> doMutate((DiscreteEntity *) parent, (DiscreteEntity *) child);
}
