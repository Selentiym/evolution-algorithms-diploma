//
// Created by nikita on 03.02.19.
//

#ifndef GENETIC_ALGORITHM_CPP_MUTATIONSABSTRACTS_H
#define GENETIC_ALGORITHM_CPP_MUTATIONSABSTRACTS_H

#include "GeneticAlgorithm.h"

class MutationCore {
public:
    virtual Entity * mutate(Entity *parent, Entity *child) = 0;
};

class DiscreteMutationCore: public MutationCore {
public:
    Entity *mutate(Entity *parent, Entity *child) override;

protected:
    virtual Entity * doMutate(DiscreteEntity *parent, DiscreteEntity *child) = 0;
};

class Mutation: public SpeciesWiseComponent {
public:
    explicit Mutation(MutationCore *mutationCore, bool replaceParents = true);

protected:
    bool replace;
    std::unique_ptr<MutationCore> core;
    void storeMutationResult(EntityNode parentNode, Entity *newEntity, Species *species);
};

class ProbabilisticMutation: public Mutation {
    /**
     * Each individual is mutated with some fixed (usually low) probability
     */
public:

    ProbabilisticMutation(MutationCore *mutationCore, bool replaceParents, double probability);

    bool speciesForward(Species *species, World *world) override;

protected:
    double proba;
};

class SingleMutation: public Mutation {
public:
    explicit SingleMutation(MutationCore *mutationCore, bool replaceParents);

    bool speciesForward(Species *species, World *world) override;
};

#endif //GENETIC_ALGORITHM_CPP_MUTATIONSABSTRACTS_H
