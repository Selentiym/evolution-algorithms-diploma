//
// Created by nikita on 03.02.19.
//

#include <cfloat>
#include "selections.h"
#include "algorithm"
#include <Eigen/Dense>

/**********************************************
 *                  Selection
 **********************************************/

Selection::Selection(bool childrenOnly, unsigned long populationSize) {
    children = childrenOnly;
    this -> populationSize = populationSize;
}

bool TournamentSelection::doSelect(Species *species, World *world, unsigned long max) {
    std::vector<double> fitnesses(species -> common -> getLength(), 0);
    unsigned long n = fitnesses.size();
    std::vector<unsigned long> rankMap(n);

    // number of times each individual was selected
    std::vector<unsigned long> toUse(n, 0);

    // get the raw fitnesses to perform true elitist selection
    extractFitnessesFromEntityList(fitnesses, species->common.get(), true);
    // need to get some number of the best individuals
    rankToIndex(fitnesses, rankMap);
    for (long i = 0; i < this -> elitists; ++i) {
        if (i >= fitnesses.size() or (i >= max)) {
            break;
        }
        ++toUse[rankMap[n-i-1]];
    }

    // collect the altered fitnesses
    extractFitnessesFromEntityList(fitnesses, species->common.get(), false);
    // sort the array once and get the rank to index map to perform selection by comparing integers
    rankToIndex(fitnesses, rankMap);

    long j, tmp, curMax, k, maxInd = max - elitists;
    if (maxInd < 0) {
        maxInd = 0;
    }
    for (j = 0; j < maxInd; ++j) {
        curMax = -1;
        for (k = 0; k < this -> toSample; ++k) {
            tmp = getRandInt(n);
            if (tmp > curMax) {
                curMax = tmp;
            }
        }
        // increment the number of individual with rank curMax
        ++ toUse[rankMap[curMax]];
    }
    j = 0;
    Node<EntityPtr> *node;
    Node<EntityPtr> *stopPointer = species -> common -> tail;//SPG
    foreach(node, species -> common) {
        // remove the entity if it has not won the tournament
        if (toUse[j] == 0) {
            species -> common -> removeNode(node);
        }
        // add as many copies of the entity as the times he won
        for (k = 1; k < toUse[j]; ++k) {
            species -> common -> push(std::shared_ptr<Entity>(EntityFactory::get() -> clone(node -> val.get()))); //SPG
        }
        ++j;
        if (node == stopPointer) {
            break;
        }
        // exit the cycle when all initial (not copied yet) entities have been iterated
        if (j >= n) {
            break;
        }
    }
//    std::cout << species -> common -> getLength() << '\n';
    return true;
}

TournamentSelection::TournamentSelection(bool childrenOnly, entityIndex populationSize, size_t tournamentSize,
                                         FitnessTransformer *transformer, size_t elitists)
        : ProportionalSpeciesSelection(childrenOnly,
                                       populationSize, transformer) {
    this -> toSample = tournamentSize;
    this -> elitists = elitists;
}

/**********************************************
 *                  Selection
 **********************************************/

bool BestReplaceWorstSelection::speciesForward(Species *species, World *world) {
//    return true;
    if (species -> common -> getLength() <= this -> populationSize) {
        return true;
    }
    //TODO: move extracting fitnesses into EntityList
    std::vector<double> fitnesses;
    fitnesses.resize(species -> common -> getLength());
    int ind = 0;
    Node<EntityPtr> *entity;
    foreach(entity, species -> common) {
        fitnesses[ind] = entity->val->getModifiedFitness();
        ++ind;
    }
    double threshold = kthLargest_sort(fitnesses, this -> populationSize);
//    double threshold = kthLargest(fitnesses, this -> populationSize); //this implementation is bad for the case when
//  there are equal elements in the array. Seems like it is the case. kthLargest_sort works much faster!
    bool flag = true;
    foreach(entity, species -> common) {
        if (flag && (entity->val->getModifiedFitness() <= threshold)) {
            species -> common -> removeNode(entity);
        }
        // so that the species would not become empty
        if (species -> common -> getLength() < 2) {
            flag = false;
        }
    }
    return true;
}

BestReplaceWorstSelection::BestReplaceWorstSelection(bool childrenOnly, unsigned long populationSize) : Selection(
        childrenOnly, populationSize) {

}

bool BestReplaceWorstSelection::GoForward(World *world) {
    return SpeciesWiseComponent::GoForward(world);
}

bool BetterFitnessSelection::GoForward(World *world){
    Node<SpeciesPtr> *speciesNode;
    Species* species;
    foreach(speciesNode, world -> population) {
        species = speciesNode -> val.get();
        auto totalSize = species->common->getLength();
        std::vector<double> fitnesses;
        fitnesses.reserve(totalSize);
        Node<EntityPtr> *node;
        foreach(node, species->common) {
            fitnesses.push_back(node->val->getModifiedFitness());
        }
//    kthLargest(fitnesses, this -> populationSize); //TODO: change
    }
}

bool ProportionalSpeciesSelection::GoForward(World *world) {
    SpeciesNode speciesNode;
    std::vector<double> fitnesses;
    extractFitnessesFromPopulation(fitnesses, world->population.get());
    if (this -> transformer != nullptr) {
        this -> transformer -> transform(fitnesses);
    }
    auto tmp = true;
    // all the species do not survive initially
    std::vector<unsigned long> survived(fitnesses.size(), 0);
    // sort the fitnesses by descending fitness
    auto indexes = sort_indexes_descending<double>(fitnesses);
    std::vector<unsigned long> lengths(fitnesses.size());

    // get the lengths of species
    unsigned long i = 0;
    foreach(speciesNode, world -> population) {
        lengths[i] = speciesNode -> val -> common -> getLength();
        ++ i;
    }

    auto saveEntity = world -> getBestIndividual();

    auto remaining = this -> populationSize - 1;
    unsigned long toSurvive;
    for (i = 0; i < fitnesses.size(); ++i) {
        toSurvive = (unsigned long)ceil(fitnesses[indexes[i]] * this -> populationSize);
        if (toSurvive > lengths[indexes[i]]) {
            toSurvive = lengths[indexes[i]];
        }
        if (toSurvive > remaining) {
            toSurvive = remaining;
        }
        survived[indexes[i]] = toSurvive;
        remaining -= toSurvive;
        if (remaining == 0) {
            break;
        }
    }

    i = 0;
    foreach(speciesNode, world -> population) {
        tmp &= this -> doSelect(speciesNode -> val.get(), world, survived[i]);
        ++ i;
    }
    world -> population -> head -> val -> common -> push(saveEntity);
    return tmp;
}

ProportionalSpeciesSelection::ProportionalSpeciesSelection(bool childrenOnly, entityIndex populationSize, FitnessTransformer *transformer)
        : Selection(
        childrenOnly, populationSize) {
    this -> transformer = std::unique_ptr<FitnessTransformer>(transformer);
}

RoulletteSelection::RoulletteSelection(bool childrenOnly, entityIndex populationSize, FitnessTransformer *transformer)
        : ProportionalSpeciesSelection(childrenOnly, populationSize, transformer) {

}

bool RoulletteSelection::doSelect(Species *species, World *world, unsigned long max) {
    std::vector<unsigned long> indices(max);
    std::vector<double> fitnesses;

    // get the fitnesses and count the sum
    extractFitnessesFromEntityList(fitnesses, species->common.get(), false);
    if (this -> transformer != nullptr) {
        this -> transformer -> transform(fitnesses);
    }
    // the total sum of fitnesses
    double sum = 0.0;
    for (auto val: fitnesses) {
        sum += val;
    }

    // generate random variables that are the pointers on the wheel
    std::vector<double> samples(max);
    for (unsigned long i = 0; i < max; ++i) {
        samples[i] = uniform();
    }
    // sort the array to make the process of collection of individuals linear
    std::sort(samples.begin(), samples.end());
    double current_threshold = 0.0;

    unsigned long toUse, k, entityIndex = 0;
    // will hold the index of the current sample
    unsigned long j = 0;
    Node<EntityPtr> *node;
    // when to stop
    Node<EntityPtr> *stopPointer = species -> common -> tail;//SPG
    bool justDelete = false;
    foreach(node, species -> common) {
        // update the cumulative upper threshold
        current_threshold += fitnesses[entityIndex] / sum;
        ++ entityIndex;
        toUse = 0;
        if (not justDelete) {
            // mark this entity as many times as the sample fell into the corresponding sector on the wheel
            // all the sectors before have already been processed, since the sorting of samples
            // so the only boundary we are looking at is the upper one
            while (samples[j] <= current_threshold) {
                ++toUse;
                ++j;
                // stop if no more samples left
                if (j >= max) {
                    break;
                }
            }
        }
        // remove the entity if it has no samples inside its sector
        if (toUse == 0) {
            species -> common -> removeNode(node);
        }
        // one copy is already inside species, add the remaining copies if needed
        for (k = 1; k < toUse; ++k) {
            species -> common -> push(std::shared_ptr<Entity>(EntityFactory::get() -> clone(node -> val.get()))); //SPG
        }
        if (node == stopPointer) {
            break;
        }
        if (j >= max) {
            justDelete = true;
        }
    }
    return true;
}

bool AverageModifier::GoForward(World *world) {
    SpeciesNode species;
    double average;
    foreach(species, world -> population) {
        average = getAverageFitness(species -> val -> common.get());
        EntityNode node;
        foreach (node, species -> val -> common.get()) {
            node -> val -> setFitness(average);
        }
    }
    return true;
}

bool DistanceModifier::GoForward(World *world) {
    SpeciesNode species, inner_species;
    EntityNode node;
    long i = -1, j = -1;
    double correction;


    Iterator<std::shared_ptr<Species>> iter_species(world -> population.get());
    Iterator<std::shared_ptr<Species>> *iter_species_ptr = &iter_species;

    // cache the fitnesses for each species, since it may change afterwards
    std::vector<double> fitnesses;
    extractFitnessesFromPopulation(fitnesses, world -> population.get());
    foreach(species, world -> population) {
        ++i;
        // for each individual in each species
        foreach (node, species -> val -> common.get()) {
            j = -1;
            correction = 0.0;
            // iterate over all other species except the same one
            foreach(inner_species, iter_species_ptr) {
                ++j;
                if (i == j) {
                    continue;
                }
                // modify the current individual's fitness
                correction += this->calculateFitnessModification(
                        this->distance(node->val.get(), inner_species->val.get()),
                        fitnesses[j],
                        node->val.get(), i, j);
            }
//            if (probaTrial(0.001)) {
//                std::cout<<(correction/node -> val -> getRawFitness())<<std::endl;
//            }
            node -> val -> setFitness(node -> val -> getRawFitness() + correction);
        }
    }
    return true;
}

DistanceModifier::DistanceModifier(DistanceMetric *metric, double alpha, long toSample, double tolerance) {
    this -> alpha = alpha;
    this -> metric = metric;
    this -> toSample = toSample;
    this -> tolerance = tolerance;
}

double DistanceModifier::distance(Entity *entity, Species *species) {
    double min = 1.0, tmp;
    EntityNode node;
    size_t counter = 0;

    Iterator<std::shared_ptr<Entity>> iter(species -> common.get());
    Iterator<std::shared_ptr<Entity>> *iter_ptr = &iter;

    foreach (node, iter_ptr) {
        tmp = this -> metric -> distance(entity, node->val.get());
        if (tmp < min) {
            min = tmp;
        }
        ++counter;
        if (counter >= this -> toSample) {
            return min;
        }
    }
    return min;
}

FitnessIndependentDistanceModifier::FitnessIndependentDistanceModifier(DistanceMetric *metric, double alpha,
                                                                       long toSample, double tolerance)
        : DistanceModifier(metric, alpha, toSample, tolerance) {

}

double FitnessIndependentDistanceModifier::calculateFitnessModification(double distance, double affectingFitness, Entity *from, unsigned long currentSpeciesIndex,
                                                                        unsigned long influenceSpeciesIndex) {
    return alpha * max(1.0 - this -> tolerance - distance, 0.0);
}

FitnessDistanceModifier::FitnessDistanceModifier(DistanceMetric *metric, double alpha, long toSample, double tolerance)
        : DistanceModifier(metric, alpha, toSample, tolerance) {

}

double FitnessDistanceModifier::calculateFitnessModification(double distance, double affectingFitness, Entity *from,
                                                             unsigned long currentSpeciesIndex,
                                                             unsigned long influenceSpeciesIndex) {
    return alpha * max(1.0 - this -> tolerance - distance, 0.0) * affectingFitness;
}

bool PredatorPreyDistanceModifier::GoForward(World *world) {
    this -> assignments.resize(world -> population -> getLength());
    this -> assigner -> assign(this -> assignments, world -> population.get());
    return DistanceModifier::GoForward(world);
}

PredatorPreyDistanceModifier::PredatorPreyDistanceModifier(DistanceMetric *metric, double gamma, long toSample,
                                                           double tolerance, PredatorPreyAssigner *assigner,
                                                           double alphaMultiplier = 0.1) : DistanceModifier(metric, gamma, toSample,
                                                                                                      tolerance) {
    this -> assigner = assigner;
    this -> alphaModifier = alphaMultiplier;
}

double PredatorPreyDistanceModifier::calculateFitnessModification(double distance, double affectingFitness, Entity *from,
                                                           unsigned long currentSpeciesIndex,
                                                           unsigned long influenceSpeciesIndex) {
    double sign = this -> calculateSign(currentSpeciesIndex, influenceSpeciesIndex);
    return alpha * max(1.0 - this -> tolerance - distance, 0.0) * affectingFitness * sign;
}

double PredatorPreyDistanceModifier::calculateSign(unsigned long currentSpeciesIndex, unsigned long influenceSpeciesIndex) {
    // 1 if the current species is predator and the second one is a prey
    // -1 if vice versa
    // 0 if both species are of the same type
    double sign = (double)this -> assignments[currentSpeciesIndex] - (double)this -> assignments[influenceSpeciesIndex];
    if (sign == 0) {
        // small repelling if the classes are the same
        sign = - this -> alphaModifier;
    }
    return sign;
}

void RandomAssigner::assign(std::vector<bool> &assignment, Population *population) {
    unsigned long j;
    for (j = 0; j < population -> getLength(); ++j) {
        assignment[j] = probaTrial(this -> proba);
    }
}

RandomAssigner::RandomAssigner(double predatorProba) {
    this -> proba = predatorProba;
}

MaxEntropyAssigner::MaxEntropyAssigner(double gamma) {
    this -> gamma = gamma;
}

void MaxEntropyAssigner::assign(std::vector<bool> &assignment, Population *population) {
    auto s = population -> getLength();
    // if there exists only one species, just put it a prey
    if (s == 1) {
        assignment[0] = false;
        return;
    }
    unsigned long N = 0, j = 0, Ni;
    double proba, mult = this -> gamma / (1 + this -> gamma);
    Node<SpeciesPtr> *speciesNode;
    long maxInd = 0;
    auto curMax = -DBL_MAX;
    // get the total number of individuals
    foreach(speciesNode, population) {
        Ni = speciesNode -> val -> common -> getLength();
        N += Ni;
        if (curMax < Ni) {
            curMax = Ni;
            maxInd = j;
        }
        ++j;
    }

    // if the maximum cluster is more than three times latger than the average without it, then remove it from the sum
    if (curMax / ((N - curMax)/(s - 1)) > 3.0) {
        N -= (unsigned long)curMax;
        s -= 1;
    } else {
        maxInd = -1;
    }
    j = 0;
    foreach(speciesNode, population) {
        // make the biggest species a prey
        if (j == maxInd) {
            assignment[j] = false;
            ++j;
            continue;
        }
        // calculate proba
        proba = mult * ((double)N / s) / speciesNode -> val -> common -> getLength();
//        std::cout << speciesNode -> val -> common -> getLength() << ", " << proba << std::endl;
        if (proba >= 1) {
            assignment[j] = true;
        } else {
            assignment[j] = probaTrial(proba);
        }
        ++j;
    }
}

SLAEModifier::SLAEModifier(DistanceMetric *metric, double alpha, long toSample, double tolerance,
                           PredatorPreyAssigner *assigner, bool changeIndividualFitness, double alphaMultiplier)
        : PredatorPreyDistanceModifier(metric, alpha, toSample,
                                       tolerance, assigner, alphaMultiplier) {
    this -> changeIndividualFitness = changeIndividualFitness;
}

bool SLAEModifier::GoForward(World *world) {
    // just duplicate the forward from predator prey. Oh, Gosh, the code starts to smell not good. No time to make
    // things clean. TODO: refactor, make the solving thing and additions calculation separate!
    this -> assignments.resize(world -> population -> getLength());
    this -> assigner -> assign(this -> assignments, world -> population.get());

    auto n = world -> population -> getLength();
    Eigen::MatrixXd m (n, n);  // the matrix for amended species fitnesses
    Eigen::VectorXd rhs (n);
//    A(3,2) = -2;

    SpeciesNode species, inner_species;
    EntityNode node;
    unsigned long i = 0;

    // cache the last individual from each species
    Entity * tmp[n];
    foreach(species, world -> population) {
        tmp[i] = species -> val -> common -> tail -> val.get();
        ++i;
    }
    i = 0;
    // cache the fitnesses for each species, since it may change afterwards
    foreach(species, world -> population) {
        // put the average fitness to the right side
        rhs(i) = species -> val -> getFitness();
        // this->distance(node->val.get(), inner_species->val.get())
        for (unsigned long j = 0; j < n; ++j) {
            if (i == j) {
                // put the one on diagonal
                m(i,j) = 1.0;
                continue;
            }
            m(i,j) = - this -> calculateSign(i, j) * this -> distance(tmp[j], species->val.get());
        }
        ++i;
    }

    // do solve the equation
    Eigen::VectorXd result = m.householderQr().solve(rhs);
    // update the fitnesses of the

    if (changeIndividualFitness) {
        return DistanceModifier::GoForward(world);
    } else {
        return true;
    }
}
