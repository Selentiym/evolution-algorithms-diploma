//
// Created by nikita on 03.02.19.
//

#ifndef GENETIC_ALGORITHM_CPP_SELECTIONS_H
#define GENETIC_ALGORITHM_CPP_SELECTIONS_H

#include "GeneticAlgorithm.h"
#include "clustering/clustering.h"

class Selection: virtual public Component {
public:

    bool children;

    explicit Selection(bool childrenOnly = false, entityIndex populationSize = 100);

protected:
    entityIndex populationSize;
};

class ProportionalSpeciesSelection: public Selection {
public:
    bool GoForward(World *world) override;

    explicit ProportionalSpeciesSelection(bool childrenOnly, entityIndex populationSize, FitnessTransformer *transformer);

protected:
    virtual bool doSelect(Species *species, World *world, unsigned long max) = 0;

    std::unique_ptr<FitnessTransformer> transformer;
};

class TournamentSelection: public ProportionalSpeciesSelection {
public:
    bool doSelect(Species *species, World *world, unsigned long max) override;

    explicit TournamentSelection(bool childrenOnly, entityIndex populationSize, size_t tournamentSize,
                                     FitnessTransformer *transformer, size_t elitists = 0);

protected:
    size_t toSample;
    size_t elitists;
};

class BestReplaceWorstSelection: public Selection, public SpeciesWiseComponent {
public:
    bool speciesForward(Species *species, World *world) override;

    bool GoForward(World *world) override;

    explicit BestReplaceWorstSelection(bool childrenOnly, entityIndex populationSize);
};

class BetterFitnessSelection: public Selection {
    BetterFitnessSelection() : Selection(false, 0) {}

public:
    bool GoForward(World *world) override;
};

class RoulletteSelection: public ProportionalSpeciesSelection {
public:
    explicit RoulletteSelection(bool childrenOnly, entityIndex populationSize, FitnessTransformer *transformer);

protected:
    bool doSelect(Species *species, World *world, unsigned long max) override;
};

class FitnessModifier: public Component {

};

class AverageModifier: public FitnessModifier {
public:
    bool GoForward(World *world) override;
};

class DistanceModifier: public FitnessModifier {
public:
    bool GoForward(World *world) override;

    explicit DistanceModifier(DistanceMetric *metric, double alpha, long toSample, double tolerance = 0.0);
protected:
    double alpha;
    DistanceMetric * metric;
    long toSample;
    double distance(Entity * entity, Species * species);
    virtual double calculateFitnessModification(double distance, double affectingFitness, Entity *from, unsigned long currentSpeciesIndex,
                                                    unsigned long influenceSpeciesIndex) = 0;
    double tolerance;
};


class FitnessIndependentDistanceModifier: public DistanceModifier {
public:
    explicit FitnessIndependentDistanceModifier(DistanceMetric *metric, double alpha, long toSample, double tolerance);

protected:
    double calculateFitnessModification(double distance, double affectingFitness, Entity *from, unsigned long currentSpeciesIndex,
                                            unsigned long influenceSpeciesIndex) override;
};


class FitnessDistanceModifier: public DistanceModifier {
public:
    explicit FitnessDistanceModifier(DistanceMetric *metric, double alpha, long toSample, double tolerance);

protected:
    double calculateFitnessModification(double distance, double affectingFitness, Entity *from, unsigned long currentSpeciesIndex,
                                            unsigned long influenceSpeciesIndex) override;
};

class PredatorPreyAssigner {
public:
    virtual void assign(std::vector<bool> &assignment, Population *population) = 0;
};

class RandomAssigner: public PredatorPreyAssigner {
public:
    void assign(std::vector<bool> &assignment, Population *population) override;

    explicit RandomAssigner(double predatorProba);

protected:
    double proba;
};

class MaxEntropyAssigner: public PredatorPreyAssigner {
public:
    void assign(std::vector<bool> &assignment, Population *population) override;

    explicit MaxEntropyAssigner(double gamma);

protected:
    double gamma;
};

class PredatorPreyDistanceModifier: public DistanceModifier {
public:
    bool GoForward(World *world) override;

    explicit PredatorPreyDistanceModifier(DistanceMetric *metric, double gamma, long toSample,
                                              double tolerance, PredatorPreyAssigner *assigner,
                                              double alphaMultiplier);

protected:

    double calculateFitnessModification(double distance, double affectingFitness, Entity *from,
                                        unsigned long currentSpeciesIndex,
                                        unsigned long influenceSpeciesIndex) override;

    double calculateSign(unsigned long currentSpeciesIndex, unsigned long influenceSpeciesIndex);

    std::vector<bool> assignments;
    PredatorPreyAssigner *assigner;
    double alphaModifier;
};


class SLAEModifier: public PredatorPreyDistanceModifier {
public:
    explicit SLAEModifier(DistanceMetric *metric, double alpha, long toSample, double tolerance,
                              PredatorPreyAssigner *assigner, bool changeIndividualFitness, double alphaMultiplier);

    bool GoForward(World *world) override;

protected:
    bool changeIndividualFitness;
};


#endif //GENETIC_ALGORITHM_CPP_SELECTIONS_H
