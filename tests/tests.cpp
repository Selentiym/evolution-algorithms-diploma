//
// Created by nikita on 17.02.19.
//

#include "clustering/clusteringAbstracts.h"
#include "GeneticAlgorithm.h"
#include "clustering/clustering.h"
#include <unordered_set>


void testArrayIterations() {
    std::vector<long> test = {1,2,3,4,5};
    long tmp;
    for (long i = 0; i < test.size(); ++i) {  // iterates added elements
        //for (auto tmp: test) {  // does not iterate added elements
        tmp = test[i];
        if (tmp == 3) {
            test.push_back(6);
            test.push_back(7);
        }
        std::cout << tmp << std::endl;
        std::cout << "size: "<< test.size() << std::endl;
    }
}

void testSetIterations() {
    std::unordered_set<long> test = {1,2,3,4,5};
    long tmp;
//    for (long i = 0; i < test.size(); ++i) {  // iterates added elements
    for (auto tmp: test) {  // does not iterate added elements
//        tmp = test[i];
        if (tmp == 3) {
            test.insert(-1);
            test.insert(-2);
        }
        std::cout << tmp << std::endl;
        std::cout << "size: "<< test.size() << std::endl;
    }
}

void testEdgeJaccard() {
    std::vector<unsigned int> v1, v2;
    v1 = {5,4,3,2,1};
    v2 = {1,3,5,2,4};
    std::unique_ptr<DistanceMetric> metric(new EdgeJaccard(v1.size()));
    DiscreteEntity entity1 = TemplatedEntity<unsigned int>(v1);
    DiscreteEntity entity2 = TemplatedEntity<unsigned int>(v2);

    std::cout << metric -> distance(&entity1, &entity2);

//    delete factory;
//    delete computer;
}

int main(int argc, char* argv[]) {
//    Entity * e1 = new Entity({0,1,2,3,4,5,6,7});
//    Entity * e2 = new Entity({0,1,2,3,4,5,6,7});
//    auto dist = new EdgeJaccard();
//    std::cout << dist -> distance(e1, e2);

//    testSetIterations();
    testEdgeJaccard();
}
